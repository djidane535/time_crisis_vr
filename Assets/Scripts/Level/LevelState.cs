﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelState : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static LevelState _instance;
    public static LevelState Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelState>();

            return _instance;
        }
    }
    #endregion

    public static int LevelNumber => Instance.levelNumber;
    public static float TopTime => Instance.topTime;

    public int levelNumber = 1;
    public float topTime;

    // String representation of the player's state.
    public override string ToString()
    {
        string str = "";
        AddLine(ref str, $"Top time: {UIDataFormat.Number.Round(TopTime, 2)}");

        return str;
    }

    // Add text line to the given string.
    private void AddLine(ref string str, string line)
    {
        str += line;
        str += "\n";
    }
}
