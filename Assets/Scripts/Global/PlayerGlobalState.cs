﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Linq;

public class PlayerGlobalState : MonoBehaviour
{
    private Transform Timelines
        => GameObject
            .FindGameObjectWithTag("Timelines")
            .transform;

    public Transform IntroTimeline
        => Timelines.Find("IntroTimeline");

    public float LastTotalTime { get; set; }

    public Transform LastCheckPointTimeline
        => Timelines
            .Cast<Transform>()
            .ToList()
            .SingleOrDefault(
                timeline => timeline.name == lastCheckpointTimelineName
            ) ?? IntroTimeline;

    [Header("Initialization settings")]
    public int initialCredits = 3;
    public int initialMaxLives = 3;

    // MaxLives
    public BoundedInteger MaxLives = new BoundedInteger(
        value: 3,
        min: 1,
        max: 5,
        defaultValue: 3
    );

    // Credits
    public BoundedInteger RemainingCredits = new BoundedInteger(
        value: 3,
        min: 0,
        max: 5
    );
    public BoundedInteger MaxCredits = new BoundedInteger(
        value: 3,
        min: 0,
        max: 5,
        defaultValue: 3
    );

    public string lastCheckpointTimelineName = "";

    private void Awake()
    {
        MaxCredits.Value = initialCredits;
        MaxLives.Value = initialMaxLives;

        if (lastCheckpointTimelineName == "")
            lastCheckpointTimelineName = IntroTimeline.name;
    }
}
