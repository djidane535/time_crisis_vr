﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public bool autoStart = false;
    public float autoStartGameDelay = .5f;
    public string levelOne;

    private GameManager GameManager
        => GameObject
            .FindGameObjectWithTag("Game")
            .GetComponent<GameManager>();

    private PlayerGlobalState PlayerGlobalState
        => GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<PlayerGlobalState>();

    private PlayerReferences PlayerReferences
        => GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerReferences>();

    private GameObject Timelines
        => GameObject
            .FindGameObjectWithTag("Timelines");

    public void TryLoadNextLevel()
    {
        // TODO to implement
        Debug.LogWarning("LevelManager.TryLoadNextLevel() is not implemented.");
        GameOver();
    }

    public void Continue()
    {
        autoStart = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameOver()
    {
        PlayerGlobalState.RemainingCredits.Value = PlayerGlobalState.MaxCredits;
        SceneManager.LoadScene(levelOne);
    }

    private void Update()
    {
        if (autoStart)
        {
            autoStart = false;
            GameManager.HideTitleScreen();
            StartCoroutine(StartGameCoroutine());
        }
    }

    private IEnumerator StartGameCoroutine()
    {
        yield return new WaitForSeconds(autoStartGameDelay);
        GameManager.Reset(ResetType.FROM_SAVE_POINT);
    }
}
