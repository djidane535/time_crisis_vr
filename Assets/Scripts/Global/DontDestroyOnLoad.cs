﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private static GameObject _instance;

    public bool isSingleton = true;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (isSingleton)
        {
            if (_instance == null) { _instance = gameObject; }
            else if (_instance != gameObject) { Destroy(gameObject); }
        }
    }
}
