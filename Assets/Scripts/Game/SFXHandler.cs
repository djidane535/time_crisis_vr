﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SFXHandler : MonoBehaviour
{
    // note: changes are only applied on SFX handler activation
    public static bool spatialize = false;
    public static float spatialBlend = 0f;

    private void OnEnable()
    {
        GetComponentsInChildren<AudioSource>()
            .ToList()
            .ForEach(
                audioSource => {
                    audioSource.spatialize = spatialize;
                    audioSource.spatialBlend = spatialBlend;
                }
            );
    }
}
