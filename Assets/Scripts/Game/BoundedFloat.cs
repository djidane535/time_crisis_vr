﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class BoundedFloat
{
    public UnityEvent OnUpdate { get; } = new UnityEvent();

    private float __value;
    public float Value
    {
        get => __value;
        set
        {
            if (Min.HasValue) { value = Mathf.Max(value, Min.Value); }
            if (Max.HasValue) { value = Mathf.Min(value, Max.Value); }
            if (__value != value)
            {
                __value = value;
                OnUpdate.Invoke();
            }
        }
    }

    public float? Min { get; private set; }

    public float? Max { get; private set; }

    public float? DefaultValue { get; private set; }

    public BoundedFloat(
            float value,
            float? min = default,
            float? max = default,
            float? defaultValue = default)
    {

        if (min.HasValue && value < min)
        {
            throw new ArgumentException(
                $"value is out of range: {value} < {min}"
            );
        }
        if (max.HasValue && value > max)
        {
            throw new ArgumentException(
                $"value is out of range: {value} > {max}"
            );
        }

        if (min.HasValue && defaultValue < min)
        {
            throw new ArgumentException(
                $"defaultValue is out of range: {defaultValue} < {min}"
            );
        }
        if (max.HasValue && defaultValue > max)
        {
            throw new ArgumentException(
                $"defaultValue is out of range: {defaultValue} > {max}"
            );
        }

        __value = value;
        Min = min;
        Max = max;
        DefaultValue = defaultValue;
    }

    public void Reset()
    {
        if (!DefaultValue.HasValue)
            throw new InvalidOperationException("No default value.");

        Value = DefaultValue.Value;
    }

    public static implicit operator float(BoundedFloat x)
    {
        return Convert.ToSingle(x.Value);
    }

    public override string ToString()
    {
        return Value.ToString();
    }
}
