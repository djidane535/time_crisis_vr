﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameState : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static GameState _instance;
    public static GameState Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameState>();

            return _instance;
        }
    }
    #endregion

    public static bool IsRunning
        => !MenuUI.IsVisible && !EndingScreens.IsVisible;

    protected GameState() { }

    // String representation of the player's state.
    public override string ToString()
    {
        string str = "";
        AddLine(ref str, $"EndingScreens is visible: {EndingScreens.IsVisible}");
        AddLine(ref str, $"Is Running: {IsRunning}");

        return str;
    }

    // Add text line to the given string.
    private void AddLine(ref string str, string line)
    {
        str += line;
        str += "\n";
    }
}
