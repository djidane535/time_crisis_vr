﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponReferences : MonoBehaviour
{
    public GameObject bulletPrefab;

    public GameObject BulletPrefab
    {
        get => bulletPrefab;
        set => bulletPrefab = value;
    }
    public Transform Model => transform.Find("Model");
    public Transform PointerAnchor => transform.Find("PointerAnchor");
    public Transform ShootParticles => PointerAnchor.Find("ShootParticles");
    public Transform SFXContainer => transform.Find("SFX");
}
