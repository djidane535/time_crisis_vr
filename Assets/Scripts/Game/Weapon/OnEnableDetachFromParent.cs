﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableDetachFromParent : MonoBehaviour
{
    private void OnEnable()
    {
        transform.SetParent(null);
    }
}
