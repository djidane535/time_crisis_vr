﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GameInput : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static GameInput _instance;
    public static GameInput Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameInput>();

            return _instance;
        }
    }
    #endregion

    public static void ReplaceInstance<T>() where T : GameInput
    {
        Destroy(Instance);
        _instance = GameObject.FindGameObjectWithTag("Game").AddComponent<T>();
    }

    public static bool GetShootDown()
        => Instance ? Instance.CustomGetShootDown() : false;

    public static Ray? GetShootingRay()
        => Instance ? Instance.CustomGetShootingRay() : null;

    public static bool GetUncoverHeld()
        => Instance ? Instance.CustomGetUncoverHeld() : false;

    protected GameInput() { }

    protected abstract bool CustomGetShootDown();
    protected abstract Ray? CustomGetShootingRay();
    protected abstract bool CustomGetUncoverHeld();
}
