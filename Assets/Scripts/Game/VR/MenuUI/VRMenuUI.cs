﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRMenuUI : MenuUI
{
    private PlayerReferences PlayerReferences
        => GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerReferences>();

    private Camera UICamera
        => GameObject
            .FindGameObjectWithTag("UICamera")
            .GetComponent<Camera>();

    protected override bool CustomUIRaycast(
            Ray ray,
            out GameObject gameObjectHit)
    {
        // Hit
        float distance = UICamera.farClipPlane;
        LayerMask mask = LayerMask.GetMask("UI"); // note: only consider layer UI
        if (Physics.Raycast(ray, out RaycastHit hit, distance, mask))
        {
            gameObjectHit = hit.collider.gameObject;
            return true;
        }

        // Miss
        gameObjectHit = null;
        return false;
    }

    // assumptions:
    //   - Initial parent is the initial checkpoint of the player
    //   - Initial position relative to this checkpoint is correctly
    //     initialized.
    private void Start()
    {
        PlayerReferences
            .TeleportHandler
            .onTeleport
            .AddListener(UpdateMenuUIPosition);
    }

    private void UpdateMenuUIPosition()
    {
        // Update MenuUI position w.r.t. the current checkpoint
        //   note: preserve its position relative to the previous checkpoint
        GameObject
            .FindGameObjectWithTag("MenuUI")
            .transform
            .SetParent(PlayerReferences.Checkpoint, false);
    }
}
