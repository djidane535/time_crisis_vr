﻿using GameFX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

public class VRSettings : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static VRSettings _instance;
    public static VRSettings Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<VRSettings>();

            return _instance;
        }
    }
    #endregion

    public static bool VRModeEnabled
        => Instance ? Instance.forceVRMode : false;

    private Camera UICamera
        => GameObject
            .FindGameObjectWithTag("UICamera")
            .GetComponent<Camera>();

    [Header("General References")]
    public Transform player;
    public GameObject desktopCamera;
    public Transform cameraShakeAnchor;

    [Header("General Settings")]
    // default: disable VR adjustements
    public bool forceVRMode = false;
    public GameObject xrRig;
    public GameObject xrInteractionSystem;    

    [Header("Audio VR Settings")]
    // default: decrease global volume and enable audio spatialization for all
    //          SFX handlers
    [Range(0f, 1f)]
    public float audioListenerVolume = .1f;
    [Range(0f, 1f)]
    public float spatialBlend = .75f;

    [Header("UI VR Settings")]
    // default: stick UI to head
    public Canvas ui;
    public float uiDistance = 1f;
    public float uiHeight = 0f;
    public float uiScale = .0003f;

    [Header("MenuUI VR Settings")]
    // default: stick MenuUI to head
    public Canvas menuUI;
    public float menuUIDistance = 10f;
    public float menuUIHeight = 2f;
    public float menuUIScale = .003f;

    [Header("FlashScreen VR Settings")]
    // default: move flash screen to cover fully player's FOV
    public Transform flashScreen;
    public float flashScreenDistance = .23f;

    [Header("DamageScreen VR Settings")]
    // default: move flash screen to cover fully player's FOV
    public Transform damageScreen;
    public float damageScreenDistance = .23f;

    [Header("Camera VR Settings")]
    // default: move camera assuming the player is seated in front of his
    //          monitor
    public Transform cameraOffset;
    public bool centerCameraOnStartup = true;
    public Vector3 cameraOffsetPosition = Vector3.down;
    public Vector3 cameraOffsetRotation = new Vector3(0f, 180f, 0f);    

    //[Header("Weapon VR Settings")]
    //// default: disable weapon equip on Player (instantiated by XRRig)

    //[Header("VRGameInput Settings")]
    //// default: set VRGameInput as the GameInput to be used.

    //[Header("VRPlayerFXPerformer Settings")]
    // default: set VRPlayerFXPerformer as the PlayerFXPerformer to be used.

    //[Header("VREnemyFXPerformer Settings")]
    // default: set VREnemyFXPerformer as the EnemyFXPerformer to be used.

    private PlayerReferences PlayerReferences
        => player.GetComponent<PlayerReferences>();

    private void Awake()
    {
        forceVRMode = forceVRMode || XRSettings.enabled;
        if (forceVRMode) { EnableXRElements(); }
    }

    private void Start()
    {
        if (!forceVRMode)
        {
            enabled = false;
            return;
        }

        HandleCameraOffset();
        HandleAudio();
        HandleUI();
        HandleMenuUI();
        HandleFlashScreen();
        HandleDamageScreen();
        HandlePlayerWeaponAnchor();
        HandleGameInput();
        HandleFXPerformers();
    }

    // Enable XR elements
    void EnableXRElements()
    {
        xrRig.SetActive(true);
        xrInteractionSystem.SetActive(true);
    }

    // Apply position / rotation offsets to VRCameraAnchor
    void HandleCameraOffset()
    {
        // Disable desktop camera
        desktopCamera.SetActive(false);

        // Center camera (if needed)
        if (centerCameraOnStartup)
            cameraOffset.GetComponent<CameraOffsetHandler>().CenterCamera();

        // Apply camera offsets
        cameraOffset.transform.localPosition += cameraOffsetPosition;
        cameraOffset.transform.localRotation = Quaternion.Euler(
            cameraOffsetRotation
        );

    }

    // Modify global volume and handle audio spatialization
    void HandleAudio()
    {
        // Global volume
        AudioListener.volume = audioListenerVolume;

        // Enable audio spatialization in all SFX handlers
        SFXHandler.spatialize = true;
        SFXHandler.spatialBlend = spatialBlend;
    }

    // Move UI
    void HandleUI()
        => SetCanvasPosition(
            ui,
            Camera.main.transform, 
            uiDistance,
            uiHeight,
            uiScale,
            Camera.main
        ); // Set UI position

    // Move MenuUI, set its sorting order to 0 and replace the current MenuUI
    // (component) by a VRMenuUI
    void HandleMenuUI()
    {
        // Hide NoVRLogo
        menuUI
            .transform
            .Find("TitleScreen")
            .Find("GameLogo")
            .Find("NoVRLogo")
            .gameObject
            .SetActive(false);

        // Set MenuUI position
        SetCanvasPosition(
            menuUI,
            PlayerReferences.Checkpoint,
            menuUIDistance,
            menuUIHeight,
            menuUIScale,
            UICamera
        );

        // Set the sorting order of MenuiUI Canvas to 0
        // (note: ordering is handled somehow by the combination of Camera
        // and UICamera)
        menuUI.sortingOrder = 0;

        // Replace the current MenuUI (component) by a VRMenuUI
        MenuUI.ReplaceInstance<VRMenuUI>();
    }

    // Move Canvas
    static void SetCanvasPosition(
            Canvas canvas,
            Transform newParent,
            float distance,
            float height,
            float scale,
            Camera worldCamera)            
    {
        canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = worldCamera; // note: worldCamera == Event camera

        canvas.transform.SetParent(newParent);
        canvas.transform.localPosition = distance * Vector3.forward;
        canvas.transform.localPosition += height * Vector3.up;
        canvas.transform.localRotation = default;
        canvas.transform.localScale = scale * Vector3.one;
    }

    // Move FlashScreen
    void HandleFlashScreen()
    {
        flashScreen.SetParent(Camera.main.transform);
        flashScreen.localPosition = flashScreenDistance * Vector3.forward;
        flashScreen.SetParent(ui.transform);
    }

    // Move DamageScreen
    void HandleDamageScreen()
    {
        damageScreen.SetParent(Camera.main.transform);
        damageScreen.localPosition = flashScreenDistance * Vector3.forward;
        damageScreen.SetParent(ui.transform);
    }

    // Disable weapon equip on Player
    void HandlePlayerWeaponAnchor()
    {
        PlayerReferences
            .LocalWeaponAnchor
            .GetComponent<OnStartEquipWeapon>()
            .enabled = false;
    }

    // Replace the current GameInput by a VRGameInput
    void HandleGameInput()
    {
        if (XRSettings.enabled) // only use VRGameInputs if VR is truly enabled
            GameInput.ReplaceInstance<VRGameInput>();
    }

    // Replace all current XXXPerformer by their VRXXXPerformer equivalent
    void HandleFXPerformers()
    {
        // VRPlayerFXPerformer
        PlayerFXPerformer.ReplaceInstance<VRPlayerFXPerformer>();

        // VREnemyFXPerformer
        EnemyFXPerformer.ReplaceInstance<VREnemyFXPerformer>();
    }
}
