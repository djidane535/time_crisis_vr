﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class VRGameInput : GameInput
{
    // GunController
    private Transform pointerAnchor;
    private static InputDevice? GunController { get; set; }

    // Devices
    private static List<InputDevice> Devices
        => GunController.HasValue ? 
            new List<InputDevice>() { GunController.Value } :
            new List<InputDevice>();
    private static Dictionary<InputDevice, bool> LastTriggerHeld { get; set; } // saves last trigger status for each device
        = new Dictionary<InputDevice, bool>();

    public VRGameInput()
    {
        InputDevices.deviceConnected += ConnectGunController;
        InputDevices.deviceDisconnected += DisconnectGunController;
    }

    private void Start()
    {
        pointerAnchor = GameObject
            .FindGameObjectWithTag("RightHandController")
            .GetComponentInChildren<WeaponReferences>()
            .PointerAnchor;
    }

    private void LateUpdate()
    {
        // Update last trigger status of all devices
        Devices
            .ForEach(
                device => LastTriggerHeld[device] = GetVRTriggerHeld(device)
            );  
    }

    private void OnDestroy()
    {
        InputDevices.deviceConnected -= ConnectGunController;
        InputDevices.deviceDisconnected -= DisconnectGunController;
    }

    protected override bool CustomGetShootDown()
        => GetVRTriggerDown(GunController);

    protected override Ray? CustomGetShootingRay()
    {
        if (pointerAnchor == null)
        {
            Debug.LogWarning("PointerAnchor reference is missing.");
            return null;
        }
        return new Ray(pointerAnchor.position, pointerAnchor.forward);
    }

    protected override bool CustomGetUncoverHeld()
        => GetVRGripButtonHeld(GunController);

    // Connect device as the new GunController if it meets the expected
    // characteristics
    private void ConnectGunController(InputDevice device)
    {
        bool deviceHasHeldInHandCharacteristic = device
            .characteristics
            .HasFlag(InputDeviceCharacteristics.HeldInHand);
        bool deviceHasRightCharacteristic = device
            .characteristics
            .HasFlag(InputDeviceCharacteristics.Right);
        bool deviceHasControllerCharacteristic = device
            .characteristics
            .HasFlag(InputDeviceCharacteristics.Controller);

        if (deviceHasHeldInHandCharacteristic &&
            deviceHasRightCharacteristic &&
            deviceHasControllerCharacteristic)
        {
            if (GunController != null)
            {
                Debug.LogWarning(
                    $"A GunController was already connected" +
                    $" (name: {GunController?.name})."
                );
            }
            GunController = device;
        }
    }

    // Disconnect the GunController if device is GunController
    private void DisconnectGunController(InputDevice device)
    {
        if (GunController == device) { GunController = null; }
    }    

    // True if the primary button of the device is pressed, false else
    private bool GetVRPrimaryButtonHeld(InputDevice? device)
    {
        if (device == null) { return false; }

        InputDevice connectedDevice = device.Value;
        return connectedDevice
            .TryGetFeatureValue(
                CommonUsages.primaryButton,
                out bool primaryButtonHeld
            ) &&
            primaryButtonHeld;
    }

    // True if the trigger of the device is pressed, false else
    private bool GetVRTriggerDown(InputDevice? device)
        => !GetVRLastTriggerHeld(GunController) &&
            GetVRTriggerHeld(GunController);

    // True if the trigger of the device is held, false else
    private bool GetVRTriggerHeld(InputDevice? device)
    {
        if (device == null) { return false; }

        // Check if the trigger is currently held
        InputDevice connectedDevice = device.Value;
        return connectedDevice
            .TryGetFeatureValue(
                CommonUsages.triggerButton,
                out bool triggerHeld
            ) &&
            triggerHeld;
    }

    // True if the trigger of the device was held during the last frame, false else
    private bool GetVRLastTriggerHeld(InputDevice? device)
        => device.HasValue &&
            LastTriggerHeld.TryGetValue(
                device.Value,
                out bool lastTriggerHeld
            ) &&
            lastTriggerHeld;

    // True if the grip button of the device is held, false else
    private bool GetVRGripButtonHeld(InputDevice? device)
    {
        if (device == null) { return false; }

        InputDevice connectedDevice = device.Value;
        return connectedDevice
            .TryGetFeatureValue(
                CommonUsages.gripButton,
                out bool gripButtonHeld
            ) &&
            gripButtonHeld;
    }
}
