﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameFX
{
    public class VREnemyFXPerformer : DesktopEnemyFXPerformer
    {
        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected override void PlayFX(Transform enemy, EnemyFX enemyFX)
        {
            switch (enemyFX)
            {
                // Do not change anything for those FX comparing to the Desktop
                // implementation
                case EnemyFX.DEATH:
                case EnemyFX.SHOOT:
                    base.PlayFX(enemy, enemyFX);
                    break;

                default:
                    Debug.LogWarning($"Unsupported FX {enemyFX}.");
                    break;
            }
        }
    }
}
