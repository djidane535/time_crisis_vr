﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOffsetHandler : MonoBehaviour
{
    [Header("References")]
    public Transform player;

    private Transform Camera => transform.Find("Camera");

    public void CenterCamera()
    {
        Vector3 offset = (player.position - Camera.position);
        Vector3 mask = new Vector3(1f, 0f, 1f);
        offset.Scale(mask);

        transform.position -= offset;
    }
}
