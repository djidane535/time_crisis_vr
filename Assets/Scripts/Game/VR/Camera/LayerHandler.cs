﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LayerHandler : MonoBehaviour
{
    public string layerName;
    public bool includeLayerWhenMenuUIIsVisible;

    private Camera Camera => GetComponent<Camera>();

    private void Update()
    {
        if ((MenuUI.IsVisible && includeLayerWhenMenuUIIsVisible) ||
                (!MenuUI.IsVisible && !includeLayerWhenMenuUIIsVisible))
            IncludeWeaponLayer();
        else
            ExcludeWeaponLayer();
    }

    private void IncludeWeaponLayer()
        => Camera.cullingMask |= LayerMask.GetMask("Weapon");

    private void ExcludeWeaponLayer()
        => Camera.cullingMask &= ~LayerMask.GetMask("Weapon");
}
