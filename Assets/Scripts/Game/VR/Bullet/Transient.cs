﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transient : MonoBehaviour
{
    public float timeToLiveInSeconds = 1f;

    private void Update()
    {
        timeToLiveInSeconds -= Time.deltaTime;
        if (timeToLiveInSeconds <= 0f) { Destroy(gameObject); }
    }
}
