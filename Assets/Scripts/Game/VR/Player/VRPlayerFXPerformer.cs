﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameFX
{
    public class VRPlayerFXPerformer : DesktopPlayerFXPerformer
    {
        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected override void PlayFX(PlayerFX playerFX)
        {
            switch (playerFX)
            {
                // Do not change anything for those FX comparing to the Desktop
                // implementation
                case PlayerFX.DEATH:
                case PlayerFX.SHOOT_FAIL:
                case PlayerFX.RELOAD:
                case PlayerFX.COVER:
                case PlayerFX.UNCOVER:
                case PlayerFX.HIT:
                    base.PlayFX(playerFX);
                    break;

                case PlayerFX.SHOOT:
                    PlaySFX(PlayerSFX.GUNSHOT);
                    PlayVFX(PlayerVFX.SHOOT_PARTICLES);
                    PlayVFX(PlayerVFX.SHOOT_BULLET);
                    break;

                default:
                    Debug.LogWarning($"Unsupported FX {playerFX}.");
                    break;
            }
        }
    }
}
