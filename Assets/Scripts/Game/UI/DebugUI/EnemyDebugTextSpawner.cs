﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDebugTextSpawner : MonoBehaviour
{
    [Header("References")]
    public GameObject debugTextPrefab;

    private Dictionary<GameObject, GameObject> debugTexts
        = new Dictionary<GameObject, GameObject>(); // associate a DebugText to an Enemy

	void OnEnable()
    {
        StartCoroutine(InstantiateMissingDebugTexts());
	}

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    // Instantiate all missing DebugText
    private IEnumerator InstantiateMissingDebugTexts()
    {
        while (true)
        {
            foreach (var enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                if (!debugTexts.ContainsKey(enemy))
                {
                    var debugText = Instantiate(debugTextPrefab, transform);

                    debugText.name = $"{debugTextPrefab.name} [{enemy.name}]";
                    var updateDebugText = debugText
                        .GetComponent<UpdateDebugText>();
                    updateDebugText.stateToDebug = enemy
                        .GetComponent<EnemyState>();
                    updateDebugText.stickToObject = true;
                    updateDebugText.fontSizeScale = .75f;
                    updateDebugText.GetComponent<Text>().enabled = false;

                    debugTexts[enemy] = debugText;                    
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }
}
