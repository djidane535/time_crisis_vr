﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateDebugText : MonoBehaviour
{
    [Header("References")]
    public MonoBehaviour stateToDebug;

    [Header("Settings")]
    public bool stickToObject = false;
    public float fontSizeScale = 1f;

    private Text debugText;
    private int initialFontSize;
    private bool fontIsSet = false;

    private void Start()
    {
        debugText = GetComponent<Text>();
        initialFontSize = debugText.fontSize;
	}

    private void Update()
    {
        // Destroy the DebugText if it's not associated to a state anymore or
        // if it's an enemy state stating the enemy is dead.
        if (stateToDebug == null
                || (stateToDebug is EnemyState
                    && (stateToDebug as EnemyState).IsDead))
        {
            Destroy(gameObject);
            return;
        }

        // Update position (if needed)
        if (stickToObject)
        {
            if (!VRSettings.VRModeEnabled) // Non-VR
            {
                // Place it in the ScreenSpace, in front of the state's
                // gameObject
                transform.position = Camera.main.WorldToScreenPoint(
                    stateToDebug.transform.position
                );
            }
            else // VR
            {
                // Place it in the WorldSpace, between the state's gameObject
                // and the camera
                Vector3 directionTowardsCamera = Vector3.Normalize(
                    Camera.main.transform.position
                        - stateToDebug.transform.position
                );
                transform.position = stateToDebug.transform.position
                    + 2f * directionTowardsCamera;
            }
        }

        // Update text content
        string str = "";
        AddLine(ref str, "[DEBUG]");
        AddLine(ref str, stateToDebug.ToString());
        debugText.text = str;

        // Update font size (once after it has been enabled)
        if (fontSizeScale > 0f && !fontIsSet)
        {
            int newFontSize = (int)Mathf.Round(initialFontSize * fontSizeScale);
            if (UnityEngine.XR.XRSettings.enabled) // VR
            {
                // Scale font w.r.t. distance
                newFontSize *= (int)Mathf.Round(
                    .5f * Vector3.Distance(
                        Camera.main.transform.position,
                        stateToDebug.transform.position
                    )
                );
            }
            debugText.fontSize = newFontSize;

            fontIsSet = true;
        }
    }

    private void OnEnable() { fontIsSet = false; }

    // Add text line
    private void AddLine(ref string str, string line)
    {
        str += line;
        str += "\n";
    }
}
