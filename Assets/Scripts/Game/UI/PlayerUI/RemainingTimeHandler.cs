﻿using Coffee.UIExtensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainingTimeHandler : MonoBehaviour
{
    // TODO urgency is not disabled if player gains time during timeline
    // TODO add an event to player state and connect UpdateRemainingTime() to it

    [Header("References")]
    public PlayerState playerState;

    private Animator animator;

    private float RemainingTimeInSeconds
        => playerState?.RemainingTime ?? 99.99f;

    private bool Urgency
        => RemainingTimeInSeconds < 10f;

    private Text secondsText;
    private Text centiSecondsText;

    private void Awake()
    {
        animator = GetComponent<Animator>();

        secondsText = transform
            .Find("Seconds")
            .GetComponent<Text>();
        centiSecondsText = transform
            .Find("CentiSeconds")
            .GetComponent<Text>();
    }

    public void UpdateRemainingTime()
    {
        secondsText.text = UIDataFormat
            .RemainingTime
            .ToSecondsString(RemainingTimeInSeconds);
        centiSecondsText.text = UIDataFormat
            .RemainingTime
            .ToCentiSecondsString(RemainingTimeInSeconds);

        // Urgency
        animator.SetBool("large_size", Urgency);
        animator.SetBool("red", Urgency);
    }

    private void OnEnable() => UpdateRemainingTime();
}
