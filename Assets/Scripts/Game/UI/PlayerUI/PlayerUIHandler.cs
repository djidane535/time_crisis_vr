﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIHandler : MonoBehaviour
{
    // This handler modifies the local scale of the PlayerUI
    // in order to show / hide it.
    // This has been done to keep all animators running
    // (as opposed to enabling / disabling the GameObject itself which resets
    // in unexpected state)

    private Animator animator;

    private void Start()
    {
        animator = transform.GetComponent<Animator>();
    }

    public void Hide()
        => animator.SetBool("hide", true);

    public void Show()
        => animator.SetBool("hide", false);
}
