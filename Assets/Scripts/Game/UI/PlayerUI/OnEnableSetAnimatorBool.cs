﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableSetAnimatorBool : MonoBehaviour
{
    public string boolName;
    public bool boolValue;

    // Start is called before the first frame update
    void OnEnable()
    {
        GetComponent<Animator>().SetBool(boolName, boolValue);
    }
}
