﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagesHandler : MonoBehaviour
{
    private bool PlayerUIIsVisible
        => GetComponentInParent<CanvasGroup>().alpha > 0f;

    public void TriggerTimeExtended()
    {
        transform
            .Find("TimeExtended")
            .GetComponent<Animator>()
            .SetTrigger("blink");
    }

    public void ShowWait()
    {
        transform
            .Find("Wait")
            .GetComponent<Animator>()
            .SetBool("visible", true);

        if (PlayerUIIsVisible)
        {
            transform
                .Find("Wait")
                .Find("SFX")
                .Find("Beep")
                .GetComponentInChildren<AudioSource>()
                .Play();
        }
    }

    public void HideWait()
    {
        transform
            .Find("Wait")
            .GetComponent<Animator>()
            .SetBool("visible", false);
    }
}
