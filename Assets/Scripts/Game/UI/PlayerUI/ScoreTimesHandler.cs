﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTimesHandler : MonoBehaviour
{
    [Header("References")]
    public PlayerState playerState;

    private Text yourTime;
    private Text topTime;

    private void Awake()
    {
        yourTime = transform
            .Find("YourTime")
            .GetComponent<Text>();
        topTime = transform
            .Find("TopTime")
            .GetComponent<Text>();
    }

    public void UpdateScoreTimes()
    {
        yourTime.text = UIDataFormat.Time.ToString(playerState.TotalTime);
        topTime.text = UIDataFormat.Time.ToString(LevelState.TopTime);
    }

    private void OnEnable() => UpdateScoreTimes();
}
