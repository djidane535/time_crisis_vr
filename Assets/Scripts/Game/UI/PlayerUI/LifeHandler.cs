﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Coffee.UIExtensions;

public class LifeHandler : MonoBehaviour
{
    [Header("References")]
    public PlayerState playerState;

    public void UpdateLifePoints()
    {
        for (int i = 0; i < transform.childCount; ++i)
        {
            transform
                .GetChild(i)
                .GetComponent<Animator>()
                .SetBool("active", i < playerState.RemainingLives);
        }
    }

    private void OnEnable() => UpdateLifePoints();
}
