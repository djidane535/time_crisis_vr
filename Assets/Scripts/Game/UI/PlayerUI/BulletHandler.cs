﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BulletHandler : MonoBehaviour
{
    [Header("References")]
    public PlayerState playerState;

    private int WeaponCapacity
        => playerState.WeaponCapacity;

    private int LastRemainingBullets { get; set; }

    private bool BulletHasBeenShot
        => CurrentActiveBulletIndex >= 0 &&
            playerState != null &&
            LastRemainingBullets > playerState.RemainingBullets;

    private bool WeaponHasBeenReloaded
        => playerState != null &&
            LastRemainingBullets < playerState.RemainingBullets;

    // Index of the current active bullet
    //   note: bullet has already been shot w.r.t. state but may not be
    //         diplayed as shot on the UI
    private int CurrentActiveBulletIndex
        => WeaponCapacity - LastRemainingBullets;

    // Index of the next active bullet
    //   note: bullet is active w.r.t. state but may not be diplayed
    //         as active on the UI
    private int NextActiveBulletIndex => CurrentActiveBulletIndex + 1;

    private void Start()
    {
        // Initialize lastRemainingBullets
        LastRemainingBullets = playerState.RemainingBullets;
    }

    public void UpdateBullets()
    {
        // Shoot
        //   note: if a bullet has been shot, the current active bullet
        //         is necessarily defined.
        //   note: CurrentActiveBulletIndex is initially negative because we
        //         assume some additional virtual bullet exists.
        //         However, since this virtual bullet does not exist,
        //         its index (-1) is not defined.
        //         Therefore, we have to check if the CurrentActiveBulletIndex
        //         references a real bullet before trying to set the trigger of
        //         its animator.
        if (BulletHasBeenShot && CurrentActiveBulletIndex >= 0)
        {
            transform
                .GetChild(CurrentActiveBulletIndex)
                .GetComponent<Animator>()
                .SetTrigger("shoot");
        }

        // Visibility, is active and reload
        for (int i = 0; i < transform.childCount; ++i)
        {
            var animator = transform
                .GetChild(i)
                .GetComponent<Animator>();

            // Visibility
            //   note: hide all bullets following the current active bullet
            animator.SetBool("hide", i < NextActiveBulletIndex);

            // Is active
            //   note: set as active the current active bullet and those
            //         preceeding it (which could still be playing an
            //         animation)
            animator.SetBool("is_active", i <= NextActiveBulletIndex);

            // Reload
            //   note: show all bullets and only set as active the first one
            if (WeaponHasBeenReloaded)
            {
                animator.SetTrigger("reload");
                animator.SetBool("is_active", i <= 0);
                animator.SetBool("hide", false);
            }
        }

        // Update last remaining bullets
        LastRemainingBullets = playerState.RemainingBullets;
    }

    private void OnEnable() => UpdateBullets();
}
