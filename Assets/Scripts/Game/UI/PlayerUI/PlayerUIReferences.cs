﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIReferences : MonoBehaviour
{
    public PlayerUIHandler Handler
        => GetComponent<PlayerUIHandler>();

    public MessagesHandler MessagesHandler
        => transform
            .Find("Messages")
            .GetComponent<MessagesHandler>();
}
