﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnablePauseGame : MonoBehaviour
{
    [Header("References")]
    public GameManager gameManager;

    [Header("Settings")]
    public bool hidePlayerUI = true;

    void OnEnable()
    {
        gameManager.Pause(hidePlayerUI);

    }
}
