﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableLoadNextLevel : MonoBehaviour
{
    public float delay = 7f;

    private LevelManager LevelManager
        => GameObject
            .Find("Global")
            .GetComponent<LevelManager>();

    private float t = 0f;

    private void OnEnable()
    {
        t = delay;
    }

    private void Update()
    {
        t -= Time.deltaTime;
        if (t <= 0f) { LevelManager.TryLoadNextLevel(); }
    }
}
