﻿using Coffee.UIExtensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsHandler : MonoBehaviour
{
    [Header("References")]
    public PlayerState playerState;

    private Text Text
        => transform.GetComponent<Text>();
    private int RemainingCredits
        => playerState?.RemainingCredits ?? 0;

    private void Update()
    {
        Text.text = $"{(int)Mathf.Clamp(RemainingCredits, 0, 9)}";
    }
}
