﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CountdownHandler : MonoBehaviour
{
    public UnityEvent OnReachingZero;
    private Transform SFX => transform.Find("SFX");

    private Text Text
        => transform
            .Find("Countdown")
            .Find("CountdownValue")
            .GetComponent<Text>();

    public void Tic(int max)
    {
        // Get current value
        int value = int.Parse(Text.text);

        // If current value is 0, invoke the event
        //   note: we do this check here in order to show value 0 on screen
        if (value == 0) { OnReachingZero.Invoke(); }

        // If the current value is <= 0, exit
        if (value <= 0) { return; }

        // Decrease the value and update the text accordingly
        Text.text = ((int)Mathf.Clamp(value - 1, 0, max)).ToString();
        SFX.Find("Tic").GetComponent<AudioSource>().Play();
    }
}
