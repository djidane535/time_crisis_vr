﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableDeactivateTarget : MonoBehaviour
{
    [Header("References")]
    public GameObject target;

	void OnEnable()
    {
        target?.SetActive(false);
	}
}
