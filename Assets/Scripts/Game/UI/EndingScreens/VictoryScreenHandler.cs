﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Events;

public class VictoryScreenHandler : MonoBehaviour
{
    [Header("References")]
    public GameManager gameManager;
    public PlayerState playerState;

    [Header("Events")]
    public UnityEvent onEnable;

    private Text stageClearValueText;
    private Text yourTimeValueText;
    private Text vsTopValueText;
    private Text accuracyValueText;
    private Text hitMissRatioText;

    private float VsTopTime
        => LevelState.TopTime - playerState.TotalTime;

    private void Awake()
    {
        stageClearValueText = gameObject
            .GetComponentsInChildren<Transform>()
            .First(child => child.name == "StageClearValue")
            .GetComponent<Text>();
        yourTimeValueText = transform
            .GetComponentsInChildren<Transform>()
            .First(child => child.name == "YourTimeValue")
            .GetComponent<Text>();
        vsTopValueText = transform
            .GetComponentsInChildren<Transform>()
            .First(child => child.name == "VsTopValue")
            .GetComponent<Text>();
        accuracyValueText = transform
            .GetComponentsInChildren<Transform>()
            .First(child => child.name == "AccuracyValue")
            .GetComponent<Text>();
        hitMissRatioText = transform
            .GetComponentsInChildren<Transform>()
            .First(child => child.name == "HitMissRatio")
            .GetComponent<Text>();
    }

    private void OnEnable()
    {
        onEnable.Invoke();
    }

    public void Pause()
    {
        gameManager.Pause(true);
    }

    public void UpdateInfo()
    {
        stageClearValueText.text = LevelState.LevelNumber.ToString();
        yourTimeValueText.text = UIDataFormat
            .Time
            .ToString(playerState.TotalTime);
        vsTopValueText.text = (VsTopTime <= 0 ? "+" : "-")
            + UIDataFormat
                .Time
                .ToString(Mathf.Abs(VsTopTime));
        accuracyValueText.text = UIDataFormat
            .Percentage
            .ToString(playerState.Accuracy);
        hitMissRatioText.text = UIDataFormat
                .Ratio
                .ToString(
                    playerState.NumberOfHits,
                    playerState.NumberOfHits + playerState.NumberOfMiss
                );
    }
}
