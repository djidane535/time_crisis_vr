﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryScreenEnabler : MonoBehaviour
{
    private PlayerState PlayeState
        => GameObject
            .FindGameObjectWithTag("Player")
            ?.GetComponent<PlayerState>();

    private bool PlayerIsDead
        => PlayeState == null || PlayeState.IsDead;

    void OnEnable()
    {
        if (!PlayerIsDead)
        {
            transform
                .Find("VictoryScreen")
                .gameObject
                .SetActive(true);
        }
    }
}
