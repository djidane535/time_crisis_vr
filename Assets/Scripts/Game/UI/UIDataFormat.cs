﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UIDataFormat
{
    public class Number
    {
        // Round float up to n digits after comma
        public static float Round(float v, int n_digits)
            => Mathf.Round(
                v * Mathf.Pow(10, n_digits)
            ) / Mathf.Pow(10, n_digits);
    }

    public class Percentage
    {
        public static string ToString(float percent)
        {
            if (percent <= 0) { return "0"; }
            if (percent >= 1) { return "100"; }
            return $"{(int)Mathf.Floor(100f * Number.Round(percent, 2))}";
        }
    }

    public class Ratio
    {
        public static string ToString(int numerator, int denominator)
            => $"({Mathf.Clamp(numerator, 0, 999)}" +
            $" / " +
            $"{Mathf.Clamp(denominator, 0, 999)})";
    }

    public class RemainingTime
    {
        public static string ToSecondsString(float timeInSeconds)
            => $"{GetSeconds(timeInSeconds)}";

        public static string ToCentiSecondsString(float timeInSeconds)
            => string.Format("{0:00}", GetCentiSeconds(timeInSeconds));

        private static int GetSeconds(float time)
        => (int)Mathf.Clamp(
            Mathf.Floor(time),
            0,
            99
        );

        private static int GetCentiSeconds(float time)
            => (int)Mathf.Clamp(
                Mathf.Round((time - GetSeconds(time)) * 100f),
                0,
                99
            );
    }

    public class Time
    {
        public static string ToString(float timeInSeconds)
            => $"{string.Format("{0:00}", GetMinutes(timeInSeconds))}'" +
            $"{string.Format("{0:00}", GetSeconds(timeInSeconds))}\"" +
            $"{string.Format("{0:00}", GetCentiSeconds(timeInSeconds))}";

        private static int GetMinutes(float time)
            => (int)Mathf.Clamp(
                Mathf.Floor(time / 60f),
                0,
                99
            );

        private static int GetSeconds(float time)
            => (int)Mathf.Clamp(
                Mathf.Floor(time - 60 * GetMinutes(time)),
                0,
                99
            );

        private static int GetCentiSeconds(float time)
            => (int)Mathf.Clamp(
                Mathf.Round(
                    (time - 60 * GetMinutes(time) - GetSeconds(time)) * 100f
                ),
                0,
                99
            );
    }
}
