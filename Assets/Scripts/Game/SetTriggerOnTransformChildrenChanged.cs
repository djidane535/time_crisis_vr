﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SetTriggerOnTransformChildrenChanged : MonoBehaviour
{
    public string tagName;
    public string triggerName;

    private void Start() => OnTransformChildrenChanged();

    private void OnTransformChildrenChanged()
    {
        transform
            .GetComponentsInChildren<Transform>()
            .Where(child => child.gameObject.CompareTag(tagName))
            .Select(child => child.GetComponent<Animator>())
            .Where(animator => animator != null)
            .ToList()
            .ForEach(animator => animator.SetTrigger(triggerName));
    }
}
