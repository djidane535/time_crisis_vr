﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartEquipWeapon : MonoBehaviour
{
    public GameObject weaponPrefab;
    public GameObject bulletPrefab;

    public string weaponTag = "Weapon";
    public bool enableModel = true;
    public bool enablePointerAnchor = true;
    public bool enableSFX = true;

    private void Start()
    {
        GameObject weapon = Instantiate(weaponPrefab, transform);
        weapon
            .transform
            .Find("Model")
            .gameObject
            .SetActive(enableModel);
        weapon
            .transform
            .Find("PointerAnchor")
            .gameObject
            .SetActive(enablePointerAnchor);
        weapon
            .transform
            .Find("SFX")
            .gameObject
            .SetActive(enableSFX);

        weapon
            .GetComponent<WeaponReferences>()
            .BulletPrefab = bulletPrefab;
        weapon.tag = weaponTag;
    }
}
