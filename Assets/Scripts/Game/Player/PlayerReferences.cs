﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReferences : MonoBehaviour
{
    public PlayerState State => GetComponent<PlayerState>();

    public Transform Checkpoint => transform.parent;
    public PlayerTeleportHandler TeleportHandler
        => GetComponent<PlayerTeleportHandler>();
    public PlayerCoverHandler CoverHandler 
        => GetComponent<PlayerCoverHandler>();
    public PlayerShootHandler ShootHandler 
        => GetComponent<PlayerShootHandler>();
    public PlayerHitHandler HitHandler
        => GetComponent<PlayerHitHandler>();

    public Animator Animator => GetComponent<Animator>();
    public Transform SFXContainer => transform.Find("SFX");

    public Transform LocalWeaponAnchor => transform.Find("WeaponAnchor");
    public WeaponReferences WeaponReferences
        => GameObject
            .FindGameObjectWithTag("PlayerWeapon")
            .GetComponentInChildren<WeaponReferences>(); // PlayerWeapon is not always a child of Player GameObject
}
