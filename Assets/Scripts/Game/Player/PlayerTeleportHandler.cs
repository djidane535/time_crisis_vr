﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTeleportHandler : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent onTeleport;

    // Teleport to the given checkpoint
    public void Teleport(Transform checkpoint)
    {
        transform.position = checkpoint.position;
        transform.rotation = checkpoint.rotation;
        transform.SetParent(checkpoint);

        onTeleport.Invoke();
    }
}
