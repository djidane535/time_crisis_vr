﻿using PlayerFXExtensions;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace GameFX
{
    public enum PlayerFX
    {
        DEATH,
        SHOOT,
        SHOOT_FAIL,
        RELOAD,
        COVER,
        UNCOVER,
        HIT,
        ACTION_VOICE
    }

    public enum PlayerVFX
    {
        WHITE_FLASH,
        RED_FLASH,
        DAMAGE_SCREEN,
        SHAKE,
        DEATH,
        SHOOT_PARTICLES,
        SHOOT_BULLET
    }

    public enum PlayerSFX
    {
        GUNSHOT,
        GUNSHOT_FAIL,
        RELOAD,
        GRUNT,
        ACTION_VOICE,
        RELOAD_VOICE
    }

    public enum PlayerAnim
    {
        COVER,
        UNCOVER
    }

    public abstract class PlayerFXPerformer : MonoBehaviour
    {
        #region SINGLETON PATTERN
        private static PlayerFXPerformer _instance;
        public static PlayerFXPerformer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<PlayerFXPerformer>();

                return _instance;
            }
        }
        #endregion

        public static void ReplaceInstance<T>() where T : PlayerFXPerformer
        {
            Destroy(Instance);
            _instance = GameObject.FindGameObjectWithTag("Game").AddComponent<T>();
        }

        private static GameObject Player
            => GameObject.FindGameObjectWithTag("Player");

        private static PlayerReferences PlayerReferences
            => Player.GetComponent<PlayerReferences>();

        private static Transform FlashScreen
            => GameObject
                .FindGameObjectWithTag("FlashScreen")
                .transform;

        private static Transform DamageScreen
            => GameObject
                .FindGameObjectWithTag("DamageScreen")
                .transform;

        private static Transform CameraShakeAnchor
            => GameObject
                .FindGameObjectWithTag("CameraShakeAnchor")
                .transform;

        // Play a PlayerFX
        public static void Play(PlayerFX playerFX)
            => Instance?.PlayFX(playerFX);

        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected abstract void PlayFX(PlayerFX playerFX);

        // Play SFX
        protected static void PlaySFX(PlayerSFX playerSFX)
        {
            // Get SFX name
            string playerSFXName;
            try { playerSFXName = playerSFX.GetName(); }
            catch (System.NotSupportedException)
            {
                Debug.LogWarning($"Unsupported SFX {playerSFX}.");
                return;
            }

            // Play SFX
            switch (playerSFX)
            {
                case PlayerSFX.GRUNT:
                    PlayerReferences
                        .SFXContainer
                        .Find(playerSFX.GetName())
                        .GetComponent<AudioSource>()
                        .Play();
                    break;

                case PlayerSFX.GUNSHOT:
                case PlayerSFX.GUNSHOT_FAIL:
                case PlayerSFX.RELOAD:
                    PlayerReferences
                        .WeaponReferences
                        .SFXContainer
                        .Find(playerSFX.GetName())
                        .GetComponent<AudioSource>()
                        .Play();
                    break;

                case PlayerSFX.ACTION_VOICE:
                case PlayerSFX.RELOAD_VOICE:
                    PlayerReferences
                        .SFXContainer
                        .Find(playerSFX.GetName())
                        .GetComponent<AudioSource>()
                        .Play();
                    break;

                default:
                    Debug.LogWarning($"Unsupported SFX {playerSFX}.");
                    break;
            }
        }

        // Play VFX
        protected static void PlayVFX(PlayerVFX playerVFX)
        {
            switch (playerVFX)
            {
                case PlayerVFX.WHITE_FLASH:
                    FlashScreen
                        .GetComponent<Animator>()
                        .SetTrigger("white_flash");
                    break;

                case PlayerVFX.RED_FLASH:
                    FlashScreen
                        .GetComponent<Animator>()
                        .SetTrigger("red_flash");
                    break;

                case PlayerVFX.DAMAGE_SCREEN:
                    DamageScreen
                        .GetComponent<Animator>()
                        .SetTrigger("hit");
                    break;

                case PlayerVFX.SHAKE:
                    CameraShakeAnchor
                        .GetComponent<CameraShake>()
                        .Shake(PlayerReferences.State.MaxStunTime);
                    break;

                case PlayerVFX.DEATH:
                    CameraShakeAnchor
                        .GetComponent<CameraShake>()
                        .Shake(1f);
                    break;

                case PlayerVFX.SHOOT_PARTICLES:
                    PlayerReferences
                        .WeaponReferences
                        .ShootParticles
                        .GetComponent<ParticleSystem>()
                        .Play();
                    break;

                case PlayerVFX.SHOOT_BULLET:
                    var weaponReferences = PlayerReferences.WeaponReferences;
                    var bullet = Instantiate(
                        weaponReferences.BulletPrefab,
                        weaponReferences.PointerAnchor.position,
                        weaponReferences.PointerAnchor.rotation
                    );
                    bullet.transform.SetParent(null);
                    break;

                default:
                    Debug.LogWarning($"Unsupported VFX {playerVFX}.");
                    break;
            }
        }

        // Play Anim
        protected static void PlayAnim(PlayerAnim playerAnim)
        {
            switch (playerAnim)
            {
                case PlayerAnim.COVER:
                    PlayerReferences
                        .Animator
                        .SetBool("covered", true);
                    break;

                case PlayerAnim.UNCOVER:
                    PlayerReferences
                        .Animator
                        .SetBool("covered", false);
                    break;

                default:
                    Debug.LogWarning($"Unsupported animation {playerAnim}.");
                    break;
            }
        }
    }
}

namespace PlayerFXExtensions
{
    public static class ToStringExtensions
    {
        public static string GetName(this GameFX.PlayerSFX playerSFX)
        {
            switch (playerSFX)
            {
                case GameFX.PlayerSFX.GUNSHOT:
                    return "Gunshot";
                case GameFX.PlayerSFX.GUNSHOT_FAIL:
                    return "GunshotFail";
                case GameFX.PlayerSFX.RELOAD:
                    return "Reload";
                case GameFX.PlayerSFX.GRUNT:
                    return "Grunt";
                case GameFX.PlayerSFX.ACTION_VOICE:
                    return "ActionVoice";
                case GameFX.PlayerSFX.RELOAD_VOICE:
                    return "ReloadVoice";
                default:
                    throw new System.NotSupportedException(
                        $"No name is associated to {playerSFX}."
                    );
            }
        }
    }
}