﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameFX
{
    public class DesktopPlayerFXPerformer : PlayerFXPerformer
    {
        private bool canPlayReloadVoice = true;

        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected override void PlayFX(PlayerFX playerFX)
        {
            switch (playerFX)
            {
                case PlayerFX.DEATH:
                    PlayVFX(PlayerVFX.DEATH);
                    break;

                case PlayerFX.SHOOT:
                    PlayVFX(PlayerVFX.WHITE_FLASH);
                    PlaySFX(PlayerSFX.GUNSHOT);
                    break;

                case PlayerFX.SHOOT_FAIL:
                    PlaySFX(PlayerSFX.GUNSHOT_FAIL);
                    if (canPlayReloadVoice)
                    {
                        PlaySFX(PlayerSFX.RELOAD_VOICE);
                        canPlayReloadVoice = false;
                    }
                    break;

                case PlayerFX.RELOAD:
                    canPlayReloadVoice = true;
                    PlaySFX(PlayerSFX.RELOAD);
                    break;

                case PlayerFX.COVER:
                    PlayAnim(PlayerAnim.COVER);
                    break;

                case PlayerFX.UNCOVER:
                    PlayAnim(PlayerAnim.UNCOVER);
                    break;

                case PlayerFX.HIT:
                    PlayVFX(PlayerVFX.RED_FLASH);
                    PlayVFX(PlayerVFX.DAMAGE_SCREEN);
                    PlayVFX(PlayerVFX.SHAKE);
                    PlaySFX(PlayerSFX.GRUNT);
                    break;

                case PlayerFX.ACTION_VOICE:
                    PlaySFX(PlayerSFX.ACTION_VOICE);
                    break;

                default:
                    Debug.LogWarning($"Unsupported FX {playerFX}.");
                    break;
            }
        }
    }
}
