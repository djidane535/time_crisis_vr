﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitHandler : MonoBehaviour
{
    private PlayerState playerState;

    private void Start()
    {
        playerState = GetComponent<PlayerState>();
    }

    // Try to hit this player.
    // Return false in case of failure.
    public bool TryHit(int damages)
    {
        if (playerState.IsCovered || playerState.IsStunned)
            return false;

        playerState.Hit(damages);
        return true;
    }
}
