﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootHandler : MonoBehaviour
{
    private PlayerState playerState;

    private void Start()
    {
        playerState = GetComponent<PlayerState>();
    }

    private void Update()
    {
        // Get a shoot ray if shoot button is down
        Ray? shootRay = GameInput.GetShootDown() ? 
            GameInput.GetShootingRay() : null;

        // Try to shoot if there is shoot ray to shoot
        if (shootRay.HasValue && TryShoot())
        {
            // No enemy has been hit
            LayerMask mask = ~LayerMask.GetMask("UI", "Weapon"); // note: ignore layer UI and Weapon
            if (!Physics.Raycast(shootRay.Value, out RaycastHit hit) ||
                !hit.collider.CompareTag("EnemyModel") ||
                !hit.collider
                    .GetComponentInParent<EnemyHitHandler>()
                    .TryHit(hit))
            {
                playerState.MissEnemy();
                return;
            }

            // Enemy hit
            playerState.HitEnemy(hit);
        }
	}

    // Try to shoot.
    // Return false in case of failure.
    public bool TryShoot()
    {
        if (playerState.IsCovered || playerState.IsStunned)
            return false;

        if (playerState.RemainingBullets <= 0)
        {
            playerState.ShootFail();
            return false;
        }

        playerState.Shoot();
        return true;
    }
}
