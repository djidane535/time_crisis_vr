﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

using GameFX;
using PlayerFXExtensions;
using UnityEngine.Events;
using System;
using System.Linq;

public class PlayerState : Optionable
{
    // Inspector
    [Header("References")]
    public GameObject cameraShakeAnchor;
    public GameObject flashScreen;
    public GameObject defeatScreen;
    public GameObject gameOverScreen;

    [Header("Initialization settings")]
    public float initialTimeInSeconds = 15f;
    public int initialHealth = 100;
    public int initialweaponCapacity = 7;
    public float initialCoverWaitingTimeInSeconds = .15f;
    public float initialUncoverWaitingTimeInSeconds = .15f;
    public float initialStunTimeInSeconds = .3f;

    public bool IsInvincible { get; set; } = false;
    public bool HasInfiniteBullets { get; set; } = false;
    public bool TimeIsStopped { get; set; } = false;

    [Header("Events")]
    public UnityEvent onRemainingTimeUpdate;
    public UnityEvent onRemainingCreditsUpdate;
    public UnityEvent onRemainingLivesUpdate;
    public UnityEvent onRemainingBulletsUpdate;
    public UnityEvent onTotalTimeUpdate;
    public UnityEvent onAddTime;

    // Additional references
    private GameManager GameManager
        => GameObject
            .FindGameObjectWithTag("Game")
            .GetComponent<GameManager>();

    private PlayerGlobalState PlayerGlobalState
        => GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<PlayerGlobalState>();
    private Transform sfx;
    private Animator animator;

    // Credits
    public BoundedInteger RemainingCredits
        => PlayerGlobalState.RemainingCredits;
    public BoundedInteger MaxCredits
        => PlayerGlobalState.MaxCredits;

    // TotalTime
    public BoundedFloat TotalTime { get; private set; } 
        = new BoundedFloat(
            value: 0f,
            min: 0f,
            max: 99f * 60f + 99.99f
        );

    // Time
    public BoundedFloat RemainingTime { get; private set; } 
        = new BoundedFloat(
            value: 15f,
            min: 0f,
            max: 99f
        );

    // Lives
    public BoundedInteger RemainingLives { get; private set; } 
        = new BoundedInteger(
            value: 3,
            min: 0,
            max: 5
        );
    public BoundedInteger MaxLives => PlayerGlobalState.MaxLives;

    // Health
    public BoundedInteger RemainingHealth { get; private set; } 
        = new BoundedInteger(
            value: 100,
            min: 0,
            max: 100
        );
    public BoundedInteger MaxHealth { get; private set; } 
        = new BoundedInteger(
            value: 100,
            min: 1,
            max: 1000,
            defaultValue: 100
        );

    // Bullets
    public BoundedInteger RemainingBullets { get; private set; } 
        = new BoundedInteger(
            value: 7,
            min: 0,
            max: 7
        );
    public BoundedInteger WeaponCapacity { get; private set; } 
        = new BoundedInteger(
            value: 7,
            min: 1,
            max: 7,
            defaultValue: 7
        );

    // Cover waiting time
    public BoundedFloat MaxCoverWaitingTime { get; private set; } 
        = new BoundedFloat(
            value: 0.15f,
            min: 0.05f,
            max: 3f
        );
    public BoundedFloat RemainingCoverWaitingTime { get; private set; } 
        = new BoundedFloat(
            value: 0f,
            min: 0f,
            max: 3f
        );

    // Uncover waiting time
    public BoundedFloat MaxUncoverWaitingTime { get; private set; } 
        = new BoundedFloat(
            value: 0.15f,
            min: 0.05f,
            max: 3f
        );
    public BoundedFloat RemainingUncoverWaitingTime { get; private set; } 
        = new BoundedFloat(
            value: 0f,
            min: 0f,
            max: 3f
        );

    // Stun time
    public BoundedFloat MaxStunTime { get; private set; } 
        = new BoundedFloat(
            value: 0.3f,
            min: 0.05f,
            max: 3f
        );
    public BoundedFloat RemainingStunTime { get; private set; } 
        = new BoundedFloat(
            value: 0f,
            min: 0f,
            max: 3f
        );

    // Status properties
    public float StunProgress
        => MaxStunTime > 0f ?
            RemainingStunTime / MaxStunTime :
            0f;

    public bool IsCovered { get; private set; }
    public bool IsStunned => RemainingStunTime > 0f;
    public bool IsDead
        => RemainingTime <= 0f || RemainingLives <= 0;

    // Hit data
    private List<RaycastHit> EnemyHits { get; set; }
        = new List<RaycastHit>();
    public int NumberOfHits => EnemyHits.Count;
    public int NumberOfMiss { get; private set; } = 0;
    public float Accuracy
        => (NumberOfHits + NumberOfMiss) > 0 ?
            NumberOfHits / (float)(NumberOfHits + NumberOfMiss) :
            0f;

    private void Awake() // note: should be initialized before anything else
    {
        sfx = transform.Find("SFX");
        animator = GetComponent<Animator>();

        // Credits
        RemainingCredits.OnUpdate.AddListener(onRemainingCreditsUpdate.Invoke);
        MaxCredits.OnUpdate.AddListener(
            delegate { RemainingCredits.Value = MaxCredits; }
        );

        // TotalTime
        TotalTime.OnUpdate.AddListener(onTotalTimeUpdate.Invoke);

        // Time
        RemainingTime.Value = initialTimeInSeconds;
        RemainingTime.OnUpdate.AddListener(onRemainingTimeUpdate.Invoke);

        // Lives
        RemainingLives.Value = MaxLives;
        RemainingLives.OnUpdate.AddListener(onRemainingLivesUpdate.Invoke);
        MaxLives.OnUpdate.AddListener(
            delegate { RemainingLives.Value = MaxLives; }
        );

        // Health
        MaxHealth.Value = initialHealth;
        MaxHealth.OnUpdate.AddListener(
            delegate { RemainingHealth.Value = MaxHealth; }
        );

        // Bullets
        WeaponCapacity.Value = initialweaponCapacity;
        RemainingBullets.OnUpdate.AddListener(onRemainingBulletsUpdate.Invoke);
        WeaponCapacity.OnUpdate.AddListener(
            delegate { RemainingBullets.Value = WeaponCapacity; }
        );        

        // Cover time
        MaxCoverWaitingTime.Value = initialCoverWaitingTimeInSeconds;
        RemainingCoverWaitingTime.Value = 0f;

        // Uncover time
        MaxUncoverWaitingTime.Value = initialUncoverWaitingTimeInSeconds;
        RemainingUncoverWaitingTime.Value = 0f;

        // Stun time
        MaxStunTime.Value = initialStunTimeInSeconds;
        RemainingStunTime.Value = 0f;
    }

    private void Update()
    {
        // State update
        if (GameState.IsRunning)
        {
            if (!TimeIsStopped)
            {
                TotalTime.Value += Time.deltaTime;
                RemainingTime.Value -= Time.deltaTime;
            }

            RemainingCoverWaitingTime.Value -= Time.deltaTime;
            RemainingUncoverWaitingTime.Value -= Time.deltaTime;
            RemainingStunTime.Value -= Time.deltaTime;
        }        

        // Death management
        if (IsDead)
        {
            // End game
            if (RemainingCredits > 0)
                GameManager.ShowDefeatScreen();
            else
                GameManager.ShowGameOverScreen();

            // VFX
            PlayerFXPerformer.Play(PlayerFX.DEATH);
        }
    }

    // Shoot
    public void Shoot()
    {
        if (!HasInfiniteBullets)
            --RemainingBullets.Value;

        PlayerFXPerformer.Play(PlayerFX.SHOOT);
    }

    // Hit an enemy
    public void HitEnemy(RaycastHit hit)
    {
        EnemyHits.Add(hit);
    }

    // Miss an enemy
    public void MissEnemy()
    {
        ++NumberOfMiss;
    }

    // Shoot (fail)
    public void ShootFail()
    {
        PlayerFXPerformer.Play(PlayerFX.SHOOT_FAIL);
    }

    // Reload
    public void Reload()
    {
        if (RemainingBullets.Value != WeaponCapacity)
        {
            RemainingBullets.Value = WeaponCapacity;
            PlayerFXPerformer.Play(PlayerFX.RELOAD);
        }
    }

    // Cover
    public void Cover()
    {
        IsCovered = true;
        RemainingHealth.Value = MaxHealth;
        RemainingUncoverWaitingTime.Value = MaxUncoverWaitingTime;

        PlayerFXPerformer.Play(PlayerFX.COVER);
    }

    // Uncover
    public void Uncover()
    {
        IsCovered = false;
        RemainingCoverWaitingTime.Value = MaxCoverWaitingTime;

        PlayerFXPerformer.Play(PlayerFX.UNCOVER);
    }

    // Hit by an enemy
    public void Hit(int damages)
    {
        if (IsInvincible) { damages = 0; }

        RemainingHealth.Value -= damages;
        if (RemainingHealth <= 0)
        {
            --RemainingLives.Value;
            RemainingHealth.Value = MaxHealth;
            RemainingStunTime.Value = MaxStunTime;

            PlayerFXPerformer.Play(PlayerFX.HIT);
        }
    }

    // Add time
    public void AddTime(float t)
    {
        if (!IsDead && t > 0f)
        {
            RemainingTime.Value += t;

            onAddTime.Invoke();
        }
    }

    // String representation of the player's state.
    public override string ToString()
    {
        string str = "";
        AddLine(ref str, $"Credits: {RemainingCredits}/{MaxCredits}");
        AddLine(ref str, "");
        AddLine(ref str, $"Invincible: {IsInvincible}");
        AddLine(ref str, $"Time stop: {TimeIsStopped}");
        AddLine(ref str, $"Infinite bullets: {HasInfiniteBullets}");
        AddLine(ref str, "");
        AddLine(ref str, $"Total time:" +
            $" {UIDataFormat.Number.Round(TotalTime, 2)}s"
        );
        AddLine(ref str, $"Remaining time:" +
            $" {UIDataFormat.Number.Round(RemainingTime, 2)}s"
        );
        AddLine(ref str, $"Lives: {RemainingLives}/{MaxLives}");
        AddLine(ref str, $"Health: {RemainingHealth}/{MaxHealth}");
        AddLine(ref str, $"Bullets: {RemainingBullets}/{WeaponCapacity}");
        AddLine(ref str, "");
        AddLine(ref str, $"Covered: {IsCovered}");
        AddLine(ref str, $" *cover wait:" +
            $" {UIDataFormat.Number.Round(RemainingCoverWaitingTime, 2)}s"
        );
        AddLine(ref str, $" *uncover wait:" +
            $" {UIDataFormat.Number.Round(RemainingUncoverWaitingTime, 2)}s"
        );
        AddLine(ref str, $"Stunned: {IsStunned}" +
            $" ({UIDataFormat.Number.Round(RemainingStunTime, 2)}s" +
            $", {UIDataFormat.Number.Round(100f * StunProgress, 2)}%)"
        );
        AddLine(ref str, $"Dead: {IsDead}");
        AddLine(ref str, "");
        AddLine(ref str, $"Number of hits: {NumberOfHits}");
        AddLine(ref str, $"Number of miss: {NumberOfMiss}");
        AddLine(ref str, $"Accuracy:" +
            $" {UIDataFormat.Number.Round(100f * Accuracy, 2)}%"
        );

        return str;
    }

    // Add text line to the given string.
    private void AddLine(ref string str, string line)
    {
        str += line;
        str += "\n";
    }

    // Optionable implementation
    public override List<string> Options
        => new List<string>() { "max_credits", "max_lives" };

    public override List<string> GetValues(string option)
    {
        switch (option)
        {
            case "max_credits":
                return Enumerable
                    .Range(MaxCredits.Min.Value, MaxCredits.Max.Value)
                    .Select(v => v.ToString())
                    .ToList();
            case "max_lives":
                return Enumerable
                    .Range(MaxLives.Min.Value, MaxLives.Max.Value)
                    .Select(v => v.ToString())
                    .ToList();
            default:
                throw new FormatException($"Unsupported option '{option}'.");
        }
    }

    public override int GetDefaultValueIndex(string option)
    {
        switch (option)
        {
            case "max_credits":
                return GetValues(option)
                    .IndexOf(MaxCredits.DefaultValue.ToString());
            case "max_lives":
                return GetValues(option)
                    .IndexOf(MaxLives.DefaultValue.ToString());
            default:
                throw new FormatException("Unsupported option.");
        }
    }

    public override int GetCurrentValueIndex(string option)
    {
        switch (option)
        {
            case "max_credits":
                return GetValues(option)
                    .IndexOf(MaxCredits.Value.ToString());
            case "max_lives":
                return GetValues(option)
                    .IndexOf(MaxLives.Value.ToString());
            default:
                throw new FormatException("Unsupported option.");
        }
    }

    public override void SetValueIndex(string option, int index)
    {
        switch (option)
        {
            case "max_credits":
                MaxCredits.Value = Int32.Parse(GetValues(option)[index]);
                break;
            case "max_lives":
                MaxLives.Value = Int32.Parse(GetValues(option)[index]);
                break;
            default:
                throw new FormatException("Unsupported option.");
        }
    }
}
