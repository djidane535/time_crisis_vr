﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoverHandler : MonoBehaviour
{
    private PlayerState playerState;

    private void Start()
    {
        playerState = GetComponent<PlayerState>();
    }

    private void Update()
    {
        if (!GameInput.GetUncoverHeld() && TryCover())
        {
            if (TryReload()) { } // TODO trigger an action/event when reloading is performed?
        }
        else if (GameInput.GetUncoverHeld() && TryUncover()) { }
    }

    // Try to cover.
    // Return false in case of failure.
    bool TryCover()
    {
        if (playerState.IsCovered 
                || playerState.IsStunned 
                || playerState.RemainingCoverWaitingTime > 0f)
            return false;

        playerState.Cover();
        return true;
    }

    // Try to uncover.
    // Return false in case of failure.
    bool TryUncover()
    {
        if (!playerState.IsCovered 
                || playerState.RemainingUncoverWaitingTime > 0f)
            return false;

        playerState.Uncover();
        return true;
    }

    // Try to reload the gun.
    // Return false in case of failure.
    public bool TryReload()
    {
        if (playerState.RemainingBullets == playerState.WeaponCapacity)
            return false;

        playerState.Reload();
        return true;
    }

    // note: triggers a reload at the beginning of a Timeline
    private void OnEnable()
    {
        if (playerState != null && TryCover())
        {
            if (TryReload()) { } // TODO trigger an action/event when reloading is performed?
        }
    }
}
