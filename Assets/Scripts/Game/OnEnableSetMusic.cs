﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnableSetMusic : MonoBehaviour
{
    [Header("References")]
    public AudioSource source;

    [Header("Settings")]
    public AudioClip clip;
    public float timeOffset = 0f;
    public bool loop = false;
    public float delay = 0f;
    public float fadeOut = 0.025f;
    public float fadeIn = 0.025f;

    private void OnEnable()
    {
        StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        if (source.clip != clip)
        {
            yield return new WaitForSeconds(delay);
            if (source.clip != null) { yield return FadeOut(); }

            source.clip = clip;
            source.time = timeOffset;
            source.loop = loop;

            if (clip != null && source.playOnAwake) { yield return FadeIn(); }
        }

        yield return null;
    }

    private IEnumerator FadeOut()
    {
        if (fadeOut > 0f)
        {
            for (float p = 1f; p > 0f; p -= Time.fixedDeltaTime)
            {
                source.volume = Mathf.Min(1f, Mathf.Max(0f, p));
                yield return new WaitForFixedUpdate();
            }
        }
        else { source.volume = 1f; }
    }

    private IEnumerator FadeIn()
    {
        source.Play();

        if (fadeIn > 0f)
        {
            for (float p = 0f; p < 1f; p += Time.fixedDeltaTime)
            {
                source.volume = Mathf.Min(1f, Mathf.Max(0f, p));
                yield return new WaitForFixedUpdate();
            }
        }
        else { source.volume = 1f; }
    }    
}
