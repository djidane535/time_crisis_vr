﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class DebugCommands : MonoBehaviour
{
    private bool ShowDebugTexts { get; set; } = false;
    private bool ShowAimingLines { get; set; } = false;

    private void Start()
    {
        StartCoroutine(HandleDebugTextsVisibility());
        StartCoroutine(HandleAimingLinesVisibility());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ToggleDebugTexts();
            TogglePlayerAimingLine();
            
        }
        if (Input.GetKeyDown(KeyCode.F2))
            ToggleAimingLines();

        if (Input.GetKeyDown(KeyCode.F5))
            HitAllEnemies();
        if (Input.GetKeyDown(KeyCode.F6))
            ToggleInvincibility();
        if (Input.GetKeyDown(KeyCode.F7))
            ToggleTimeStop();
        if (Input.GetKeyDown(KeyCode.F8))
            ToggleInfiniteBullets();

        if (Input.GetKeyDown(KeyCode.F9))
            Continue();
        if (Input.GetKeyDown(KeyCode.F10))
            GameOver();
        if (Input.GetKeyDown(KeyCode.F12))
            ReloadScene();
    }

    // Show / Hide DebugTexts
    private void ToggleDebugTexts()
    {
        ShowDebugTexts = !ShowDebugTexts;
    }

    // Show / Hide player aiming line
    private void TogglePlayerAimingLine()
    {
        LineRenderer aimingLine = GameObject
            .FindGameObjectWithTag("PlayerWeapon")
            ?.transform
            .Find("PointerAnchor")
            .Find("AimingLine")
            .GetComponent<LineRenderer>();

        if (aimingLine) { aimingLine.enabled = !aimingLine.enabled; }
    }

    // Show / Hide aiming lines (all except PlayerWeapon)
    private void ToggleAimingLines()
    {
        ShowAimingLines = !ShowAimingLines;
    }

    // Hit all enemies 
    private void HitAllEnemies()
    {
        GameObject
            .FindGameObjectsWithTag("Enemy")
            .ToList()
            .Select(enemy => enemy.GetComponent<EnemyState>())
            .ToList()
            .ForEach(enemyState => enemyState.Hit(default)); // TODO ensure default is a correct value
    }

    // Enable / disable invincibility
    private void ToggleInvincibility()
    {
        var playerState = GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerState>();

        playerState.IsInvincible = !playerState.IsInvincible;
    }

    // Enable / disable time
    private void ToggleTimeStop()
    {
        var playerState = GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerState>();

        playerState.TimeIsStopped = !playerState.TimeIsStopped;
    }

    // Enable / disable infinite bullets
    private void ToggleInfiniteBullets()
    {
        var playerState = GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerState>();

        playerState.HasInfiniteBullets = !playerState.HasInfiniteBullets;
    }

    // Continue
    private void Continue()
    {
        var levelManager = GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<LevelManager>();

        levelManager.Continue();
    }

    // Try continue
    private void GameOver()
    {
        var levelManager = GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<LevelManager>();

        levelManager.GameOver();
    }

    // Reload scene
    private void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Coroutine for showing / hiding DebugTexts
    private IEnumerator HandleDebugTextsVisibility()
    {
        while (true)
        {
            GameObject
                .FindGameObjectsWithTag("DebugText")
                .ToList()
                .Select(debugText => debugText.GetComponent<Text>())
                .ToList()
                .ForEach(text => text.enabled = ShowDebugTexts);

            yield return null;
        }
    }

    // Coroutine for showing / hiding aiming lines (all except PlayerWeapon)
    private IEnumerator HandleAimingLinesVisibility()
    {
        while (true)
        {
            GameObject
                .FindGameObjectsWithTag("AimingLine")
                .Select(aimingLine => aimingLine.GetComponent<LineRenderer>())
                .ToList()
                .Where(
                    aimingLine 
                        => !aimingLine
                                .transform
                                .parent // PointerAnchor
                                .parent // <weapon>
                                .CompareTag("PlayerWeapon"))
                .ToList()
                .ForEach(aimingLine => aimingLine.enabled = ShowAimingLines);

            yield return null;
        }
    }
}
