﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowTarget : MonoBehaviour
{
    [Header("References")]
    public Transform target;

    [Header("Settings")]
    public bool onlyOnActiveSelfTarget = false;
    public bool onlyOnActiveInHierarchyTarget = true;
    public bool followPosition = true;
    public bool followRotation = false;

    private void Update()
    {
        bool targetActiveSelf = target.gameObject.activeSelf;
        bool targetActiveInHierarchy = target.gameObject.activeInHierarchy;

        if ((!onlyOnActiveSelfTarget && !onlyOnActiveInHierarchyTarget) ||
            (onlyOnActiveSelfTarget && targetActiveSelf) ||
            (onlyOnActiveInHierarchyTarget && targetActiveInHierarchy))
        {
            if (followPosition) { transform.position = target.position; }
            if (followRotation) { transform.rotation = target.rotation; }
        }
    }
}
