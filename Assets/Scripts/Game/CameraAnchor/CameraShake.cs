﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    private Vector3 originalPos;

    private void Start()
    {
        originalPos = transform.localPosition;
    }

    // Shake camera
    public void Shake(float shakeDuration)
    {
        StartCoroutine(ShakeCoroutine(shakeDuration));
    }

    // Shake camera coroutine
    IEnumerator ShakeCoroutine(float shakeDuration)
    {
        while (shakeDuration > 0)
        {
            transform.localPosition = originalPos +
                Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.fixedDeltaTime * decreaseFactor;

            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        transform.localPosition = originalPos;
    }
}
