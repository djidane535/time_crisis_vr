﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBullet : MonoBehaviour
{
    private ParticleSystem shootParticles;

    public GameObject bulletPrefab;
    public float delay = 1f;

    private void Awake()
    {
        shootParticles = GetComponentInChildren<ParticleSystem>();
    }

    private void Start()
    {
        StartCoroutine(Launch());
    }

    private IEnumerator Launch()
    {
        while (true)
        {
            shootParticles.Play();
            Instantiate(bulletPrefab, transform.position, transform.rotation);
            yield return new WaitForSeconds(delay);
        }
    }
}
