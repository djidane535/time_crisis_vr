﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedSpawner : MonoBehaviour
{
    [Header("References")]
    public GameObject objectPrefab;

    [Header("Settings")]
    public float delayInSeconds;

    private void Start()
    {
        // Spawn object in 'delayInSeconds' seconds.
        Invoke("Spawn", delayInSeconds);
	}

    // Spawn object
    void Spawn()
    {
        var gameObject = Instantiate(objectPrefab, transform);
        gameObject.name = $"{objectPrefab.name} (from {transform.name})";
    }
}
