﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Optionable : MonoBehaviour
{
    // List of options
    public abstract List<string> Options { get; }

    // Return the ordered list of values of an option
    public abstract List<string> GetValues(string option);

    // Return the index of the default value of an option
    public abstract int GetDefaultValueIndex(string option);

    // Return the index of the current value of an option
    public abstract int GetCurrentValueIndex(string option);

    // Set the value index of an option
    public abstract void SetValueIndex(string option, int index);
}
