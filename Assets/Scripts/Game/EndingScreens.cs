﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class EndingScreens
{
    public static bool IsVisible
        => GameObject.Find("EndingScreens")
            .transform
            .Cast<Transform>()
            .Select(child => child.gameObject)
            .Any(gameObject => gameObject.activeInHierarchy);
}
