﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDisableAddTime : MonoBehaviour
{
    [Header("References")]
    public PlayerState playerState;

    [Header("Settings")]
    public float timeToAdd = 0f;

    private void OnDisable()
    {
        playerState.AddTime(timeToAdd);
    }
}
