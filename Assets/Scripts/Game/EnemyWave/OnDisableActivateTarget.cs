﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDisableActivateTarget : MonoBehaviour
{
    [Header("References")]
    public GameObject target;
    public bool useActiveSelf = false;

	void OnDisable()
    {
        // Skip case
        if (useActiveSelf && gameObject.activeSelf) { return; }

        if (target != null) { target.SetActive(true); }
	}
}
