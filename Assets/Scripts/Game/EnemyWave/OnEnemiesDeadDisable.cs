﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class OnEnemiesDeadDisable : MonoBehaviour
{
    private bool AllEnemiesSpawned
        => transform
            .Cast<Transform>()
            .ToList()
            .All(child => child.childCount >= 1);

    private bool AllEnemiesDead
        => GameObject
            .FindGameObjectsWithTag("Enemy")
            .ToList()
            .Select(enemy => enemy.GetComponent<EnemyState>())
            .ToList()
            .All(enemyState => enemyState.IsDead);

	void LateUpdate()
    {
        if (AllEnemiesSpawned && AllEnemiesDead)
            gameObject.SetActive(false);
	}
}
