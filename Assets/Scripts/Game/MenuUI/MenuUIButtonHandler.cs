﻿using GameFX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
// Custom inspector which:
//   - Adds a button "Trigger hit" for simulating a hit on the button
[CustomEditor(typeof(MenuUIButtonHandler), true)]
public class MenuUIButtonHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MenuUIButtonHandler handler = (target as MenuUIButtonHandler);

        if (GUILayout.Button("Trigger hit"))
        {
            if (Application.isPlaying) { handler.HitButton(); }
            else
            {
                Debug.LogWarning(
                    $"Cannot trigger hit on '{handler.name}' in editor."
                );
            }
        }

        base.OnInspectorGUI();
    }
}
#endif

public abstract class MenuUIButtonHandler : MonoBehaviour
{
    private void Update()
    {
        // Skip if the MenuUI is not visible or the player did not shoot
        if (!MenuUI.IsVisible ||
                !GameInput.GetShootDown()) { return; }

        // Play FX
        PlayerFXPerformer.Play(PlayerFX.SHOOT);

        // Check if the player hit the button
        // (see https://gamedev.stackexchange.com/a/93606)
        Ray? shootRay = GameInput.GetShootingRay();
        if (shootRay.HasValue &&
                MenuUI.Raycast(shootRay.Value, out GameObject gameObjectHit) &&
                gameObjectHit == gameObject)
        {
            HitButton();
        }
    }

    public abstract void HitButton();
}
