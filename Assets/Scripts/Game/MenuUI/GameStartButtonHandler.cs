﻿using GameFX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameStartButtonHandler : MenuUIButtonHandler
{
    [Header("References")]
    public GameManager gameManager;

    [Header("Settings")]
    public float delay = .5f;

    private Animator MenuAnimator
        => transform.GetComponentInParent<Animator>();

    public override void HitButton()
    {
        StartCoroutine(HitButtonCoroutine());
    }

    private IEnumerator HitButtonCoroutine()
    {
        // Hide animation
        MenuAnimator.SetBool("hide", true);
        yield return new WaitForSeconds(delay);

        // Disable parent
        transform.parent.gameObject.SetActive(false);

        // Start game
        gameManager.Reset(ResetType.FROM_START);
    }
}
