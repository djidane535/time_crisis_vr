﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DesktopMenuUI : MenuUI
{
    private GraphicRaycaster GraphicRaycaster
        => GameObject
            .FindGameObjectWithTag("MenuUI")
            .transform
            .Cast<Transform>()
            .ToList()
            .Single(child => child.GetComponent<CanvasGroup>().alpha > 0f)
            .GetComponent<GraphicRaycaster>();            

    protected override bool CustomUIRaycast(
            Ray ray,
            out GameObject gameObjectHit)
    {
        // Preparation for raycast
        PointerEventData ped = new PointerEventData(null);
        ped.position = Camera.main.WorldToScreenPoint(ray.origin);

        List<RaycastResult> results = new List<RaycastResult>();
        GraphicRaycaster.Raycast(ped, results);

        // Hit
        if (results.Count == 1)
        {
            gameObjectHit = results[0].gameObject;
            return true;
        }

        // Miss
        gameObjectHit = null;
        return false;
    }
}
