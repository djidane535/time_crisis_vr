﻿using GameFX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DefeatButtonHandler : MenuUIButtonHandler
{
    public enum DefeatButtonType { CONTINUE, GAME_OVER }

    public float delay = 1f;
    public DefeatButtonType buttonType = DefeatButtonType.CONTINUE;

    private Animator MenuAnimator
        => transform.GetComponentInParent<Animator>();

    private GameManager GameManager
        => GameObject
            .FindGameObjectWithTag("Game")
            .GetComponent<GameManager>();

    private LevelManager LevelManager
        => GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<LevelManager>();

    private Action ButtonAction
    {
        get
        {
            switch (buttonType)
            {
                case DefeatButtonType.CONTINUE:
                    return LevelManager.Continue;
                case DefeatButtonType.GAME_OVER:
                default:
                    return GameManager.ShowGameOverScreen;
            }
        }
    }

    public override void HitButton()
    {
        StartCoroutine(DelayedAction(ButtonAction, delay));
        MenuAnimator.SetBool("hide", true);
        StartCoroutine(
            SetActiveGameObject(
                transform.parent.gameObject,
                false,
                delay + .15f // TODO
            )
        );
    }    

    private static IEnumerator SetActiveGameObject(
            GameObject target,
            bool active,
            float delay)
    {
        yield return new WaitForSeconds(delay);
        target.SetActive(active);
    }

    private static IEnumerator DelayedAction(Action action, float delay)
    {
        yield return new WaitForSeconds(delay);
        action();
    }
}
