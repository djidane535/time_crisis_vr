﻿using GameFX;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NavigationButtonHandler : MenuUIButtonHandler
{
    public float delay = 1f;
    public GameObject toEnableOnHit;

    private Animator MenuAnimator
        => transform.GetComponentInParent<Animator>();

    public override void HitButton()
    {
        StartCoroutine(SetActiveGameObject(toEnableOnHit, true, delay));
        MenuAnimator.SetBool("hide", true);
        StartCoroutine(
            SetActiveGameObject(
                transform.parent.gameObject,
                false,
                delay + .15f // TODO
            )
        );
    }

    private static IEnumerator SetActiveGameObject(
            GameObject target,
            bool active,
            float delay)
    {
        yield return new WaitForSeconds(delay);
        target.SetActive(active);
    }
}
