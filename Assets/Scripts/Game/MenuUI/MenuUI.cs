﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class MenuUI : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static MenuUI _instance;
    public static MenuUI Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MenuUI>();

            return _instance;
        }
    }
    #endregion

    public static void ReplaceInstance<T>() where T : MenuUI
    {
        Destroy(Instance);
        _instance = GameObject.FindGameObjectWithTag("Game").AddComponent<T>();
    }

    public static bool IsVisible
        => GameObject
            .FindGameObjectWithTag("MenuUI")
            .transform
            .Cast<Transform>()
            .Select(child => child.GetComponent<CanvasGroup>())
            .Any(canvasGroup => canvasGroup.alpha > 0f);

    public static bool Raycast(Ray ray, out GameObject gameObjectHit)
    {
        if (Instance == null) { gameObjectHit = null; return false; }
        return Instance.CustomUIRaycast(ray, out gameObjectHit);
    }

    protected MenuUI() { }

    protected abstract bool CustomUIRaycast(
        Ray ray,
        out GameObject gameObjectHit
    );
}
