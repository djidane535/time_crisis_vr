﻿using GameFX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum OptionButtonType { DEFAULT, PREVIOUS, NEXT }

// TODO move up this script for handling the whole thing (previous/next/default)
public class OptionButtonHandler : MenuUIButtonHandler
{
    public OptionButtonType type;
    

    private OptionFieldHandler OptionFieldHandler
        => GetComponentInParent<OptionFieldHandler>();

    private Optionable Optionable => OptionFieldHandler.Optionable;
    private string Option => OptionFieldHandler.Option;
    private Text Text => OptionFieldHandler.Text;

    public override void HitButton()
    {
        // Get current value index
        int index = Optionable.GetCurrentValueIndex(Option);

        // Compute new index
        List<string> values = Optionable.GetValues(Option);
        switch (type)
        {
            case OptionButtonType.DEFAULT:
                index = Optionable.GetDefaultValueIndex(Option);
                break;
            case OptionButtonType.PREVIOUS:
                --index;
                break;
            case OptionButtonType.NEXT:
                ++index;
                break;
        }
        index = (int)Mathf.Clamp(index, 0, values.Count - 1);

        // Set new value index
        Optionable.SetValueIndex(Option, index);

        // Update option value in Text
        index = Optionable.GetCurrentValueIndex(Option);
        Text.text = values[index];
    }
}
