﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionFieldHandler : MonoBehaviour
{
    public Optionable optionable;
    public string option;

    public Optionable Optionable => optionable;
    public string Option => option;
    public Text Text => transform.Find("OptionValue").GetComponent<Text>();

    private void OnEnable()
    {
        // Update option value in Text w/ its initial value
        int index = Optionable.GetCurrentValueIndex(Option);
        List<string> values = Optionable.GetValues(option);
        Text.text = values[index].ToString();
    }
}
