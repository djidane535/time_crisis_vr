﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum ResetType
{
    FROM_START,
    FROM_SAVE_POINT
}

public class GameManager : MonoBehaviour
{
    [Header("References")]
    public PlayerUIReferences playerUIReferences;
    public PlayerReferences playerReferences;
    public GameObject enemyWaves;
    public GameObject titleScreen;
    public GameObject defeatScreen;
    public GameObject gameOverScreen;
    public GameObject timelines;

    [Header("Settings")]
    public float startGameDelay = .5f;

    private PlayerGlobalState PlayerGlobalState
        => GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<PlayerGlobalState>();

    private PlayerReferences PlayerReferences
        => GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerReferences>();

    private Transform Timelines
        => GameObject
            .FindGameObjectWithTag("Timelines")
            .transform;

    public void Reset(ResetType resetType)
    {
        // Use one credit
        --PlayerGlobalState.RemainingCredits.Value;

        // Disable all timelines
        Timelines
            .Cast<Transform>()
            .ToList()
            .ForEach(timeline => timeline.gameObject.SetActive(false));

        // Enable the correct timeline
        switch (resetType)
        {
            case ResetType.FROM_START:
                PlayerGlobalState.IntroTimeline.gameObject.SetActive(true);
                break;

            case ResetType.FROM_SAVE_POINT:
                PlayerReferences.State.TotalTime.Value = 
                    PlayerGlobalState.LastTotalTime;
                PlayerGlobalState.LastCheckPointTimeline.gameObject.SetActive(true);
                break;
        }
    }    

    public void Pause(bool hidePlayerUI)
    {
        if (hidePlayerUI) { HidePlayerUI(); }
        DisconnectPlayer();
        StartCoroutine(DisconnectEnemyWavesCoroutine());
    }

    public void Unpause(bool showPlayerUI)
    {
        if (showPlayerUI) { ShowPlayerUI(); }
        ConnectPlayer();
        StartCoroutine(ConnectEnemyWavesCoroutine());
    }

    public void HideTitleScreen()
    {
        titleScreen.GetComponent<CanvasGroup>().alpha = 0f;
        titleScreen.gameObject.SetActive(false);
    }

    public void ShowDefeatScreen()
    {
        defeatScreen.SetActive(true);
        DisableElementsWhenShowingScreen();
    }

    public void ShowGameOverScreen()
    {
        gameOverScreen.SetActive(true);
        DisableElementsWhenShowingScreen();
    }

    private void DisableElementsWhenShowingScreen()
    {
        playerReferences.State.enabled = false;
        enemyWaves.SetActive(false);
        timelines.SetActive(false);
    }

    private void ShowPlayerUI()
    {
        playerUIReferences.Handler.Show();
    }

    private void HidePlayerUI()
    {
        playerUIReferences.Handler.Hide();
    }

    private void ConnectPlayer()
    {
        playerReferences.CoverHandler.enabled = true;
        playerReferences.ShootHandler.enabled = true;
        playerReferences.HitHandler.enabled = true;
    }

    private void DisconnectPlayer()
    {
        playerReferences.CoverHandler.enabled = false;
        playerReferences.ShootHandler.enabled = false;
        playerReferences.HitHandler.enabled = false;
    }

    // Note:
    //   This method could be called from a sequence triggered by
    //   EnemyWaves.OnDisable().
    //   Unfortunately, it is not allowed to call SetActive() method during the
    //   call of OnDisable().
    //   Therefore, this coroutine delay the call to SetActive() to avoid any
    //   conflicts.
    private IEnumerator ConnectEnemyWavesCoroutine()
    {
        yield return new WaitForEndOfFrame();

        enemyWaves.SetActive(true);
        yield return null;
    }

    private IEnumerator DisconnectEnemyWavesCoroutine()
    {
        yield return new WaitForEndOfFrame();

        enemyWaves.SetActive(false);
        yield return null;
    }
}
