﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameFX
{
    public class DesktopEnemyFXPerformer : EnemyFXPerformer
    {
        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected override void PlayFX(Transform enemy, EnemyFX enemyFX)
        {
            switch (enemyFX)
            {
                case EnemyFX.DEATH:
                    PlayVFX(enemy, EnemyVFX.BULLET_IMPACT);
                    PlaySFX(enemy, EnemySFX.GRUNT);
                    PlayAnim(enemy, EnemyAnim.DEATH);
                    break;

                case EnemyFX.SHOOT:
                    PlaySFX(enemy, EnemySFX.GUNSHOT);
                    PlayVFX(enemy, EnemyVFX.SHOOT_PARTICLES);
                    PlayVFX(enemy, EnemyVFX.SHOOT_BULLET);
                    break;

                default:
                    Debug.LogWarning($"Unsupported FX {enemyFX}.");
                    break;
            }
        }
    }
}
