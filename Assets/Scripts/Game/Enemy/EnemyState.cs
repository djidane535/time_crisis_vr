﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameFX;
using EnemyFXExtensions;

public class EnemyState : MonoBehaviour
{
    [Header("Initialization Settings")]
    public int initialDamages = 25;
    public float initialStunTimeInSeconds = 1f;
    public float iniitalDeathTimeInSeconds = 1f;

    // Damages
    public BoundedInteger DamagesPerHit { get; private set; }
        = new BoundedInteger(
            value: 25,
            min: 0,
            max: 999
        );

    // Stun time
    public BoundedFloat RemainingStunTime { get; private set; }
        = new BoundedFloat(
            value: 0f,
            min: 0f,
            max: 3f
        );
    public BoundedFloat MaxStunTime { get; private set; }
        = new BoundedFloat(
            value: 1f,
            min: 0.05f,
            max: 3f
        );

    // Status properties
    public float StunProgress
        => MaxStunTime > 0f ?
            RemainingStunTime / MaxStunTime :
            0f;

    public bool Dying { get; private set; }
    public bool IsStunned => RemainingStunTime > 0f;
    public bool IsDead => Dying && !IsStunned;

    // Hit data
    public RaycastHit? LastHit { get; set; }

    private void Start()
    {
        DamagesPerHit.Value = initialDamages;
        RemainingStunTime.Value = 0f;
        MaxStunTime.Value = initialStunTimeInSeconds;
        Dying = false;
    }

    private void Update()
    {
        RemainingStunTime.Value -= Time.deltaTime;
    }

    // Shoot
    public void Shoot()
    {
        EnemyFXPerformer.Play(transform, EnemyFX.SHOOT);
    }

    // Hit a player
    public void HitPlayer() { }

    // Hit by a player
    public void Hit(RaycastHit hit)
    {
        RemainingStunTime.Value = MaxStunTime;
        Dying = true;
        LastHit = hit;

        EnemyFXPerformer.Play(transform, EnemyFX.DEATH);
    }

    // String representation of the player's state.
    public override string ToString()
    {
        string str = "";
        AddLine(ref str, $"Damages per hit: {DamagesPerHit}");
        AddLine(ref str, "");
        AddLine(ref str, $"Stunned: {IsStunned}" +
            $" ({Round(RemainingStunTime, 2)}s" +
            $", {Round(100f * StunProgress, 2)}%)");
        AddLine(ref str, $"Dying: {Dying}");
        AddLine(ref str, $"Dead: {IsDead}");
        AddLine(ref str, "");
        AddLine(ref str, $"Last hit: {LastHit}");

        return str;
    }

    // Add text line to the given string.
    private void AddLine(ref string str, string line)
    {
        str += line;
        str += "\n";
    }

    // Round float up to n digits after comma
    private float Round(float v, int n_digits)
        => Mathf.Round(v * Mathf.Pow(10, n_digits)) / Mathf.Pow(10, n_digits);
}
