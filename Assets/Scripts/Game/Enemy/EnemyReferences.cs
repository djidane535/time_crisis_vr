﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyReferences : MonoBehaviour
{
    public EnemyState State => GetComponent<EnemyState>();
    public Animator Animator => GetComponent<Animator>();
    public Transform SFXContainer => transform.Find("SFX");
    public Transform VFXContainer => transform.Find("VFX");
    public WeaponReferences WeaponReferences
        => transform
            .Find("WeaponAnchor")
            .GetComponentInChildren<WeaponReferences>();
}
