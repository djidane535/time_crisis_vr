﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using System.Linq;

using EnemyFXExtensions;

namespace GameFX
{
    public enum EnemyFX
    {
        DEATH,
        SHOOT
    }

    public enum EnemyVFX
    {
        SHOOT_PARTICLES,
        SHOOT_BULLET,
        BULLET_IMPACT
    }

    public enum EnemySFX
    {
        GUNSHOT,
        GRUNT
    }

    public enum EnemyAnim
    {
        DEATH
    }

    public abstract class EnemyFXPerformer : MonoBehaviour
    {
        #region SINGLETON PATTERN
        private static EnemyFXPerformer _instance;
        public static EnemyFXPerformer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<EnemyFXPerformer>();

                return _instance;
            }
        }
        #endregion

        public static void ReplaceInstance<T>() where T : EnemyFXPerformer
        {
            Destroy(Instance);
            _instance = GameObject.FindGameObjectWithTag("Game").AddComponent<T>();
        }

        private static EnemyReferences GetEnemyReferences(Transform enemy)
            => enemy.GetComponent<EnemyReferences>();

        // Play an EnemyFX
        public static void Play(Transform enemy, EnemyFX enemyFX)
            => Instance?.PlayFX(enemy, enemyFX);

        // Play FX
        //   Entrypoint for performing VFX, SFX and animations depending on the
        //   the implementation of the FX by the Instance (Desktop, VR, ...).
        protected abstract void PlayFX(Transform enemy, EnemyFX enemyFX);

        // Play SFX
        protected static void PlaySFX(Transform enemy, EnemySFX enemySFX)
        {
            // Get SFX name
            string enemySFXName;
            try { enemySFXName = enemySFX.GetName(); }
            catch (System.NotSupportedException)
            {
                Debug.LogWarning($"Unsupported SFX {enemySFX}.");
                return;
            }

            // Play SFX
            switch (enemySFX)
            {
                // Enemy-related SFX
                case EnemySFX.GRUNT:
                    GetEnemyReferences(enemy)
                        .SFXContainer
                        .Find(enemySFX.GetName())
                        .GetComponent<AudioSource>()
                        .Play();
                    break;

                // Weapon-related SFX
                case EnemySFX.GUNSHOT:
                    GetEnemyReferences(enemy)
                        .WeaponReferences
                        .SFXContainer
                        .Find(enemySFX.GetName())
                        .GetComponent<AudioSource>()
                        .Play();
                    break;

                default:
                    Debug.LogWarning($"Unsupported SFX {enemySFX}.");
                    break;
            }
        }

        // Play VFX
        protected static void PlayVFX(Transform enemy, EnemyVFX enemyVFX)
        {
            switch (enemyVFX)
            {
                case EnemyVFX.SHOOT_PARTICLES:
                    GetEnemyReferences(enemy)
                        .WeaponReferences
                        .ShootParticles
                        .GetComponentInChildren<ParticleSystem>()
                        .Play();
                    break;

                case EnemyVFX.SHOOT_BULLET:
                    var weaponReferences = GetEnemyReferences(enemy)
                        .WeaponReferences;
                    var bullet = Instantiate(
                        weaponReferences.BulletPrefab,
                        weaponReferences.PointerAnchor.position,
                        weaponReferences.PointerAnchor.rotation
                    );
                    bullet.transform.SetParent(null);
                    break;

                case EnemyVFX.BULLET_IMPACT:
                    var enemyReferences = GetEnemyReferences(enemy);
                    
                    // Get hit point
                    Vector3? hitPoint = enemyReferences
                        .State
                        .LastHit
                        ?.point;

                    // Exit if no hit point
                    if (hitPoint == null)
                    {
                        Debug.LogWarning(
                            $"Unable to play {enemyVFX} (no hit point)."
                        );
                        return;
                    }

                    // Get the requested VFX
                    Transform vfx = enemyReferences
                        .VFXContainer
                        .Find(enemyVFX.GetName());

                    // Move the VFX w.r.t. the hit point
                    vfx.position = hitPoint.Value + .1f * enemy.transform.forward;

                    // Play the particle system associated to the VFX
                    vfx.GetComponent<ParticleSystem>().Play();

                    break;

                default:
                    Debug.LogWarning($"Unsupported VFX {enemyVFX}.");
                    break;
            }
        }

        // Play Anim
        protected static void PlayAnim(Transform enemy, EnemyAnim enemyAnim)
        {
            switch (enemyAnim)
            {
                case EnemyAnim.DEATH:
                    var animator = GetEnemyReferences(enemy).Animator;
                    animator.SetTrigger("hit");
                    animator.SetTrigger("dying");
                    break;

                default:
                    Debug.LogWarning($"Unsupported AnimFX {enemyAnim}.");
                    break;
            }
        }
    }
}

namespace EnemyFXExtensions
{
    public static class GetNameExtensions
    {
        public static string GetName(this GameFX.EnemySFX enemySFX)
        {
            switch (enemySFX)
            {
                case GameFX.EnemySFX.GUNSHOT:
                    return "Gunshot";
                case GameFX.EnemySFX.GRUNT:
                    return "Grunt";
                default:
                    throw new System.NotSupportedException(
                        $"No name is associated to {enemySFX}."
                    );
            }
        }

        public static string GetName(this GameFX.EnemyVFX enemyVFX)
        {
            switch (enemyVFX)
            {
                case GameFX.EnemyVFX.BULLET_IMPACT:
                    return "ImpactParticles";
                default:
                    throw new System.NotSupportedException(
                        $"No name is associated to {enemyVFX}."
                    );
            }
        }
    }
}