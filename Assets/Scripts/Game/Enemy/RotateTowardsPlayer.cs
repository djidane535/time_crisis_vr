﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// c/c from https://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
public class RotateTowardsPlayer : MonoBehaviour
{
    private PlayerReferences playerReferences;
    private EnemyReferences enemyReferences;

    public float speed = 1.0f;
    public float noise = 5f;

    private void Start()
    {
        enemyReferences = GetComponent<EnemyReferences>();
    }

    private void Update()
    {
        PlayerReferences playerReferences = GameObject
            .FindGameObjectWithTag("Player")
            ?.GetComponent<PlayerReferences>();

        // Exit if no player
        if (playerReferences == null)
        {
            Debug.LogWarning(
                $"{gameObject.transform.name}" +
                $" (parent: {gameObject.transform.parent.name}): " +
                $" no player found."
            );
            return;
        }

        // Exit if no weapon
        if (enemyReferences.WeaponReferences == null)
        {
            Debug.LogWarning(
                $"{gameObject.transform.name}" +
                $" (parent: {gameObject.transform.parent.name}): " +
                $" no weapon found."
            );
            return;
        }

        // Determine which direction to rotate towards
        Transform origin = enemyReferences.WeaponReferences.PointerAnchor;
        Transform target = Camera.main.transform;        
        Vector3 targetDirection = target.position - origin.position;

        // Add noise to target direction if it's not a decisive shot
        EnemyState enemyState = enemyReferences.State;
        PlayerState playerState = playerReferences.State;
        if (!IsNextShotDecisive(enemyState, playerState))
        {
            targetDirection += Random.Range(-noise, +noise)
                * (Time.deltaTime * 60f)
                * transform.right;
        }

        // The step size is equal to speed times frame time.
        float singleStep = speed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(
            transform.forward, 
            targetDirection, 
            singleStep,
            0.0f
        );

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    // True if the next shot is decisive, false else
    private static bool IsNextShotDecisive(
            EnemyState enemyState,
            PlayerState playerState)
    => enemyState.DamagesPerHit >= playerState.RemainingHealth;
}
