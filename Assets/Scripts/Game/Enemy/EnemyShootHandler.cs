﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootHandler : MonoBehaviour
{
    private EnemyState enemyState;
    private PlayerHitHandler playerHitHandler;

    public float shootsPerSecond = 1f;

    private void Start()
    {
        enemyState = GetComponent<EnemyState>();
        playerHitHandler = GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerHitHandler>();
    }

    public IEnumerator ShootCoroutine()
    {
        yield return new WaitForSeconds(shootsPerSecond/3f);

        while (true)
        {
            // note: This coroutine is only called when the enemy is uncovered
            if (TryShoot() && playerHitHandler.TryHit(enemyState.DamagesPerHit))
                enemyState.HitPlayer();

            yield return new WaitForSeconds(shootsPerSecond);
        }
    }

    // Try to shoot.
    // Return false in case of failure.
    public bool TryShoot()
    {
        if (enemyState.IsStunned)
            return false;

        enemyState.Shoot();
        return true;
    }
}
