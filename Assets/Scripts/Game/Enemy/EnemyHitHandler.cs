﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitHandler : MonoBehaviour
{
    private EnemyState enemyState;
    private Collider enemyCollider;

    private void Start()
    {
        enemyState = GetComponent<EnemyState>();
        enemyCollider = GetComponent<Collider>();
	}
	
    // Try to hit this enemy.
    // Return false in case of failure.
	public bool TryHit(RaycastHit hit)
    {
        if (enemyState.IsStunned || enemyState.IsDead)
            return false;

        enemyState.Hit(hit);
        return true;
    }
}
