﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDieDisable : MonoBehaviour
{
    private EnemyState enemyState;

    private void Start()
    {
        enemyState = GetComponent<EnemyState>();
    }
	
	private void LateUpdate()
    {
        if (enemyState.IsDead)
            gameObject.SetActive(false);
	}
}
