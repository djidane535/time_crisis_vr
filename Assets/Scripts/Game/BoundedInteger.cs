﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class BoundedInteger
{
    public UnityEvent OnUpdate { get; } = new UnityEvent();

    private int __value;
    public int Value
    {
        get => __value;
        set
        {
            if (Min.HasValue) { value = Mathf.Max(value, Min.Value); }
            if (Max.HasValue) { value = Mathf.Min(value, Max.Value); }
            if (__value != value)
            {
                __value = value;
                OnUpdate.Invoke();
            }
        }
    }

    public int? Min { get; private set; }

    public int? Max { get; private set; }

    public int? DefaultValue { get; private set; }

    public BoundedInteger(
            int value,
            int? min = default,
            int? max = default,
            int? defaultValue = default)
    {

        if (min.HasValue && value < min)
        {
            throw new ArgumentException(
                $"value is out of range: {value} < {min}"
            );
        }
        if (max.HasValue && value > max)
        {
            throw new ArgumentException(
                $"value is out of range: {value} > {max}"
            );
        }

        if (min.HasValue && defaultValue < min)
        {
            throw new ArgumentException(
                $"defaultValue is out of range: {defaultValue} < {min}"
            );
        }
        if (max.HasValue && defaultValue > max)
        {
            throw new ArgumentException(
                $"defaultValue is out of range: {defaultValue} > {max}"
            );
        }

        __value = value;
        Min = min;
        Max = max;
        DefaultValue = defaultValue;
    }

    public void Reset()
    {
        if (!DefaultValue.HasValue)
            throw new InvalidOperationException("No default value.");

        Value = DefaultValue.Value;
    }

    public static implicit operator int(BoundedInteger x)
    {
        return Convert.ToInt32(x.Value);
    }

    public override string ToString()
    {
        return Value.ToString();
    }
}
