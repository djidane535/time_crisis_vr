﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesktopGameInput : GameInput
{
    protected override bool CustomGetShootDown()
        => Input.GetMouseButtonDown(0);

    protected override Ray? CustomGetShootingRay()
        => Camera.main.ScreenPointToRay(Input.mousePosition);

    protected override bool CustomGetUncoverHeld()
        => Input.GetMouseButton(1);
}
