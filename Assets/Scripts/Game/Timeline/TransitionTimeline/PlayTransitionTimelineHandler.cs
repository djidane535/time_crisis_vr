﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

using GameFX;

public class PlayTransitionTimelineHandler : MonoBehaviour
{
    [Header("References")]
    public GameManager gameManager;
    public PlayerUIReferences playerUIReferences;
    public PlayerReferences playerReferences;
    public Transform originCheckpoint;
    public Transform destinationCheckpoint;

    // For begin signal
    public void MovePlayerToOriginCheckpoint()
    {
        playerReferences.TeleportHandler.Teleport(originCheckpoint);
    }

    public void ShowWaitMessage()
    {
        playerUIReferences.MessagesHandler.ShowWait();
    }

    // For end signal
    public void MovePlayerToDestinationCheckpoint()
    {
        playerReferences.TeleportHandler.Teleport(destinationCheckpoint);
    }

    public void HideWaitMessage()
    {
        playerUIReferences.MessagesHandler.HideWait();
    }    

    public void ResetPlayerAnimator()
    {
        playerReferences.Animator.Play("Hub");
        playerReferences.Animator.ResetTrigger("crouch_cover");
        playerReferences.Animator.ResetTrigger("stand_left_cover");
        playerReferences.Animator.ResetTrigger("stand_right_cover");
    }

    public void Unpause()
    {
        // notes:
        //   (1) PlayerUI is hidden on startup
        //   (2) PlayerUI is shown at the end of the first timeline
        //   (3) PlayerUI will not be hidden by any other timeline
        gameManager.Unpause(true);
    }

    public void PlayStartSFX()
    {
        StartCoroutine(PlayStartSFXCoroutine());
    }

    private IEnumerator PlayStartSFXCoroutine()
    {
        yield return new WaitForSeconds(.33f); // crouching time

        PlayerFXPerformer.Play(PlayerFX.ACTION_VOICE);
        PlayerFXPerformer.Play(PlayerFX.RELOAD);
    }
}
