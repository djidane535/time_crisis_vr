﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class InitTransitionTimelineHandler : MonoBehaviour
{
    [Header("References")]
    public GameManager gameManager;
    public PlayerReferences playerReferences;

    public void Pause()
    {
        gameManager.Pause(false);
    }

    public void TriggerPlayerUncover()
    {
        playerReferences.State.Uncover();
    }
}
