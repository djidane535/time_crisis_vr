﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointHandler : MonoBehaviour
{
    private PlayerGlobalState PlayerGlobalState
        => GameObject
            .FindGameObjectWithTag("Global")
            .GetComponent<PlayerGlobalState>();

    private PlayerState PlayerState
        => GameObject
            .FindGameObjectWithTag("Player")
            .GetComponent<PlayerReferences>()
            .State;

    private void OnEnable()
    {
        PlayerGlobalState.lastCheckpointTimelineName = transform.name;
        PlayerGlobalState.LastTotalTime = PlayerState.TotalTime.Value;
    }
}
